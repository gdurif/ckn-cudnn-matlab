#include <linalg.h>
#include <mexutils.h>

template <typename T>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[]) {
   Matrix<T> X;
   getMatrix(prhs[0],X);
   const int m = X.m();
   plhs[0]=createMatrix<T>(m,m);
   Matrix<T> U;
   getMatrix(plhs[0],U);
   plhs[1]=createMatrix<T>(m,1);
   Vector<T> S;
   getVector(plhs[1],S);
   Matrix<T> Y;
   Y.copy(X);
   Y.SymEig(U,S);
}



void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 1)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 2)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs);
   } else {
      callFunction<float>(plhs,prhs);
   }
}

