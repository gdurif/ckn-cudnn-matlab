#include <mexutils.h>

template <typename T>
inline void swap_scalar(T& X, T& Y) {
   T tmp=X;
   X=Y;
   Y=tmp;
}

template <typename T>
inline void swap(T* X, T* Y, T* buff, const int n) {
   memcpy(buff,Y,n*sizeof(T));
   memcpy(Y,X,n*sizeof(T));
   memcpy(X,buff,n*sizeof(T));
};

template <typename T>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nlhs) {
   Matrix<T> X;
   getMatrix<T>(prhs[0],X);
   const INTM m = X.m();
   const INTM n = X.n();
   T* prX=X.rawX();
   Vector<int> per;
   getVector<int>(prhs[1],per);
   T* buff= new T[m];
   INTM* who = new INTM[n];
   INTM* where = new INTM[n];
   for (INTM j = 0; j<n; ++j) who[j]=j;
   for (INTM j = 0; j<n; ++j) where[j]=j;
   for (INTM i=0; i<n; ++i) {
      const INTM j=where[per[i]];
      swap<T>(prX+i*m,prX+j*m,buff,m);
      swap_scalar<INTM>(where[who[i]],where[who[j]]);
      swap_scalar<INTM>(who[i],who[j]);
   }
   delete[](buff);
   delete[](who);
   delete[](where);
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 2)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 0)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nlhs);
   } else {
      callFunction<float>(plhs,prhs,nlhs);
   }
}

