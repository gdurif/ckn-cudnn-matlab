#include <linalg.h>
#include <mexutils.h>
#include "common.h"

template <typename Tin, typename T>
inline void callFunctionAux(mxArray* plhs[], const int nlhs, mxArray* pr_layers, const int nlayers, const Map<Tin>& map, const bool verbose = true) {
   Layer<T> layers[nlayers];
   for (int ii=0; ii<nlayers; ++ii) {
      mxArray* layer=mxGetCell(pr_layers,ii);
      layers[ii].num_layer=ii+1;
      layers[ii].npatch=getScalarStruct<int>(layer,"npatch");
      layers[ii].nfilters=getScalarStruct<int>(layer,"nfilters");
      layers[ii].subsampling=getScalarStruct<int>(layer,"subsampling");
      layers[ii].stride=getScalarStructDef<int>(layer,"stride",1);
      layers[ii].zero_padding=getScalarStructDef<bool>(layer,"zero_padding",false);
      layers[ii].type_layer=getScalarStruct<int>(layer,"type_layer");
      layers[ii].type_kernel=getScalarStruct<int>(layer,"type_kernel");
      layers[ii].sigma=getScalarStruct<T>(layer,"sigma");
      mxArray *pr_W2 = mxGetField(layer,0,"W2");
      getMatrix(pr_W2,layers[ii].W2);
      mxArray *pr_W = mxGetField(layer,0,"W");
      getMatrix(pr_W,layers[ii].W);
      mxArray *pr_b = mxGetField(layer,0,"b");
      getVector(pr_b,layers[ii].b);
      mxArray *pr_Wfilt = mxGetField(layer,0,"Wfilt");
      getMatrix(pr_Wfilt,layers[ii].Wfilt);
      mxArray *pr_mu = mxGetField(layer,0,"mu");
      getVector(pr_mu,layers[ii].mu);
   };
   Map<Tin> map_zero;
   map.refSubMapZ(0,map_zero);
   Map<T> map_zero_out;
   encode_ckn_map(map_zero,layers,nlayers,map_zero_out,verbose);
   const INTM ndesc=map_zero_out.x()*map_zero_out.y()*map_zero_out.z();
   if (verbose) {
      PRINT_I(map_zero_out.x())
      PRINT_I(map_zero_out.y())
      PRINT_I(map_zero_out.z())
   }
   plhs[0]=createMatrix<T>(ndesc,map.z());
   Matrix<T> psi;
   getMatrix(plhs[0],psi);
#ifdef TIMINGS
   RESET_TIMERS
#endif
   encode_ckn(map,layers,nlayers,psi);
#ifdef TIMINGS
   PRINT_TIMERS
#endif
};

template <typename Tin>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nlhs) {
   Map<Tin> map;
   getMap(prhs[0],map);
   mxArray *pr_layers = mxGetField(prhs[1],0,"layer");
   const mwSize* dims_layer=mxGetDimensions(pr_layers);
   const int nlayers=dims_layer[0]*dims_layer[1];
   bool double_precision = getScalarStructDef<bool>(prhs[2],"double_precision",false);
   int threads = getScalarStructDef<int>(prhs[2],"threads",-1);
   bool verbose = getScalarStructDef<bool>(prhs[2],"verbose",true);
   if (threads == -1) {
      threads=1;
#ifdef _OPENMP
      threads =  MIN(MAX_THREADS,omp_get_num_procs());
#endif
   } 
   threads=init_omp(threads);
#ifdef HAVE_MKL
   vmlSetMode(0x00000003 | 0x00280000 | 0x00000100);
#endif
   if (double_precision) {
      callFunctionAux<Tin,double>(plhs,nlhs,pr_layers,nlayers,map,verbose);
   } else {
      callFunctionAux<Tin,float>(plhs,nlhs,pr_layers,nlayers,map,verbose);
   }
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 3)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 1)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nlhs);
   } else if (mxGetClassID(prhs[0]) == mxUINT8_CLASS) {
      callFunction<unsigned char>(plhs,prhs,nlhs);
   } else {
      callFunction<float>(plhs,prhs,nlhs);
   }
}
