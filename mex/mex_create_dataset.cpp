#include <linalg.h>
#include <mexutils.h>
#include "common.h"

template <typename Tin, typename T>
inline void callFunctionAux(mxArray* plhs[], mxArray* pr_layers, const int nlayers, const int ntrain, const T max_memory, const Map<Tin>& map, const int nlhs) {
   Layer<T> layers[nlayers];
   for (int ii=0; ii<nlayers; ++ii) {
      mxArray* layer=mxGetCell(pr_layers,ii);
      layers[ii].num_layer=ii+1;
      layers[ii].npatch=getScalarStruct<int>(layer,"npatch");
      layers[ii].nfilters=getScalarStruct<int>(layer,"nfilters");
      layers[ii].subsampling=getScalarStruct<int>(layer,"subsampling");
      layers[ii].stride=getScalarStructDef<int>(layer,"stride",1);
      layers[ii].zero_padding=getScalarStructDef<bool>(layer,"zero_padding",false);
      layers[ii].type_layer=getScalarStruct<int>(layer,"type_layer");
      layers[ii].type_kernel=getScalarStruct<int>(layer,"type_kernel");
      /// whitening is done subsequently
      if (ii < nlayers-1) {
         layers[ii].sigma=getScalarStruct<T>(layer,"sigma");
         mxArray *pr_W = mxGetField(layer,0,"W");
         getMatrix(pr_W,layers[ii].W);
         mxArray *pr_b = mxGetField(layer,0,"b");
         getVector(pr_b,layers[ii].b);
         mxArray *pr_Wfilt = mxGetField(layer,0,"Wfilt");
         getMatrix(pr_Wfilt,layers[ii].Wfilt);
         mxArray *pr_mu = mxGetField(layer,0,"mu");
         getVector(pr_mu,layers[ii].mu);
         mxArray *pr_W2 = mxGetField(layer,0,"W2");
         getMatrix(pr_W2,layers[ii].W2);
      }
   }
   /// query the descriptor size and initialize the memory
   Map<Tin> map_zero;
   map.refSubMapZ(0,map_zero);
   Map<T> map_zero_out;
   encode_ckn_map(map_zero,layers,nlayers-1,map_zero_out);

   const INTM e = layers[nlayers-1].npatch;
   const INTM m = (map_zero_out.y()-e+1)*(map_zero_out.z()-e+1);
   const INTM n = map.z();
   const INTM ndesc=e*e*map_zero_out.x();
   const INTM max_ntrain = MIN(ntrain,floor((max_memory*1024*1024*1024)/(4*ndesc)));
   const INTM per_image = MIN(floor(max_ntrain/T(n)),m);
   printf("Number of patches to extract: %d\n",per_image*n);
   plhs[0]=createMatrix<T>(ndesc,per_image*n);
   Matrix<T> X;
   getMatrix(plhs[0],X);
   Vector<int> labels;
   if (nlhs>=2) {
      plhs[1]=mxCreateNumericMatrix(static_cast<mwSize>(per_image*n),1,mxINT32_CLASS,mxREAL);
      getVector<int>(plhs[1],labels);
   }
/*   Vector<T> nrms;
   if (nlhs>=3) {
      plhs[2]=mxCreateMatrix(per_image*n,1);
      getVector<T>(plhs[2],nrms);
   }*/
   extract_dataset(map,layers,nlayers,X,labels);
};

template <typename Tin>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nlhs) {
   Map<Tin> map;
   getMap(prhs[0],map);
   mxArray *pr_layers = mxGetField(prhs[1],0,"layer");
   bool double_precision = getScalarStructDef<bool>(prhs[2],"double_precision",false);
   const int nlayers=getScalarStruct<int>(prhs[2],"current_layer");
   const int seed=getScalarStructDef<int>(prhs[2],"seed",-1);
   if (seed >= 0)
      srandom(seed);
   const double max_memory=getScalarStructDef<double>(prhs[2],"max_memory",10000.0);
   const int ntrain=getScalarStruct<int>(prhs[2],"ntrain");
   int threads = getScalarStructDef<int>(prhs[2],"threads",-1);
   if (threads == -1) {
      threads=1;
#ifdef _OPENMP
      threads =  MIN(MAX_THREADS,omp_get_num_procs());
#endif
   } 
   threads=init_omp(threads);
   if (double_precision) {
      callFunctionAux<Tin,double>(plhs,pr_layers,nlayers,ntrain,max_memory,map,nlhs);
   } else {
      callFunctionAux<Tin,float>(plhs,pr_layers,nlayers,ntrain,max_memory,map,nlhs);
   }
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 3)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 1 && nlhs != 2 && nlhs != 3)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nlhs);
   } else if (mxGetClassID(prhs[0]) == mxUINT8_CLASS) {
      callFunction<unsigned char>(plhs,prhs,nlhs);
   } else {
      callFunction<float>(plhs,prhs,nlhs);
   }
}
