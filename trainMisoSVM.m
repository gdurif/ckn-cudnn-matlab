function [acc] = trainMisoSVM(Xtr,Ytr,Xte,Yte,namesvm,threads,seed);
if isa(Xtr,'single')
   Ytr=single(Ytr);
   Yte=single(Yte);
end
tabC=2.^(-15:15);
acc=zeros(1,length(tabC));
nclasses=max(Ytr)+1;
tic
param.eps=1e-4;
param.accelerated=true;
param.threads=threads;
param.seed=seed;

if exist(namesvm)
   load(namesvm);
end

for ii=1:length(tabC)
   if (~acc(ii))
      [tmp ind]=max(acc);
      if (ii >= 10 && ind <= ii - 4)
         break;
      end
      n=size(Xtr,2);
      param.max_it=1000*n;
      param.lambda=1.0/(2*n*tabC(ii));
      tic
      W=mex_svm_miso((Ytr),Xtr,param);
      toc
      pred=W'*Xte;
      [tmp ind]=max(pred);
      acc(ii)=length(find(Yte==(ind'-1)))/length(Yte);
      acc
   end
end
acc
toc
if ~isempty(namesvm)
   save(namesvm,'acc');
end
end


