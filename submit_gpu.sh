#!/bin/bash

#OAR -n supervised_training_ckn
#OAR -l walltime=48:0:0
#OAR -p gpumodel='p100'
#OAR -O /home/thoth/gdurif/code/ckn-cudnn-matlab/log_submit_gpu_supervised0.out
#OAR -E /home/thoth/gdurif/code/ckn-cudnn-matlab/log_submit_gpu_supervised0.out

GPU_ID=$(gpu_getIDs.sh)
echo "HOST=$(hostname)"
echo "GPU_ID=$GPU_ID"

cd $HOME/code/ckn-cudnn-matlab/

bash run_matlab_cmd.sh script_ckn_supervised
