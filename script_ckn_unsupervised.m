npatches=[3 3];  
subsampling=[2 6];

%nfilters=[64 256]; 
% dimension 2304, obtains around 77.2% accuracy on cifar-10 with single model and no data augmentation

nfilters=[256 1024]; 
% dimension 9216, obtains around 82% with [3,2]

% nfilters=[512 8192];
% dimension 73728, obtains around 85% accuracy with [3,2]

%nfilters=[1024 16384]; 
% dimension 147456, obtains around 85.7% accuracy with [3,3] 

%nfilters=[2048 32768]; 
%dimension 294912, obtains around 86.2% accuracy with [3,3] 
% can we get more with higher dimensions??

sigmas=[0.6];
type_kernel=0;
zero_pad=1;
lambda2=0.01;
centering=1;
whitening=1;
gradients=0;
type_learning=0; % 0: k-means on 1000000 patches
                 % 1: k-means on 100000 patches
                 % 2: k-means on 100000 patches from 10% data
                 % 3: k-means on 100000 patches from 1% data
                 % 4: random patches (noise), no k-means
                 % 5: no k-means, random patches from data.
                 % 6: no k-means, random patches from 10% data.
                 % 7: no k-means, random patches from 1% data.
                 % 8: 2 epochs k-means from 1% data.

device='mkl'; % unsupervised learning is not coded on gpus
threads=8;
dataset='cifar-10';
train_ckn_unsupervised(npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,gradients,type_learning,lambda2,device,threads,dataset);

