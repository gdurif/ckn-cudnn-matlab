function model = training_unsupervised(Input,Ytr,param,dataset)
npatches=param.npatches;
nfilters=param.nfilters;
type_kernel=param.type_kernel;
subsampling=param.subsampling;
sigmas=param.sigmas;
zero_pad=param.zero_pad;
lambda2=param.lambda2;

model.nlayers=nnz(npatches);
nlayers=model.nlayers;
model.layer=cell(nlayers,1);

for ii=1:nlayers
   fprintf('Training of layer %d\n',ii);
   model_layer.npatch=npatches(ii);
   model_layer.sigma=param.sigmas(ii)
   model_layer.subsampling=(subsampling(ii));
   model_layer.stride=1;
   model_layer.nfilters=nfilters(ii);
   model_layer.zero_padding= zero_pad;
   model_layer.W=single(0);
   model_layer.b=single(0);
   model_layer.W2=single(0);
   model_layer.W3=single(0);
   model_layer.Wfilt=single(0);
   model_layer.mu=single(0);
   model_layer.type_kernel=type_kernel(ii);
   
   if (ii==1)
      if param.whitening
         model_layer.type_layer=5;
      elseif param.centering
         model_layer.type_layer=1;
      elseif param.gradients; 
         model_layer.type_layer=4;
      else
         model_layer.type_layer=0;
      end
   else
      model_layer.type_layer=0;
   end

   fprintf('Create the training set\n');
   param_dataset.ntrain=param.ntrain;
   param_dataset.max_memory=30; % in GB
   param_dataset.current_layer=ii;
   param_dataset.threads=param.threads;
   model.layer{ii}=model_layer;

   if (param.ntrain > 0) 
      if param.num_train_images==-1
         X=mex_create_dataset(Input,model,param_dataset);
      else
         per=randperm(size(Input,3));
         X=mex_create_dataset(Input(:,:,per(1:param.num_train_images)),model,param_dataset);
      end

      nrms=mex_normalize(X);
      ind=find(nrms);
      X=X(:,ind);

      % do permutation
      per=randperm(size(X,2));
      mex_permutation(X,int32(per-1));

      % learn W and b
      fprintf('Train the filters\n');
      param_approx.d=model_layer.nfilters;
      param_approx.threads=param.threads;
      param_approx.num_iter=param.num_iter_kmeans;
      Z = mex_kmeans(X,param_approx);
      ind=find(sum(Z.^2)==0);
      if ~isempty(ind)
         Z(:,ind)=rand(size(Z,1),length(ind),'single');
         Z(:,ind)=(bsxfun(@rdivide,Z(:,ind),sqrt(sum(Z(:,ind).^2))));
      end
   else
      param_dataset.ntrain=100;
      X=mex_create_dataset(Input(:,:,1:2),model,param_dataset);
      Z=randn(size(X,1),model_layer.nfilters);
      Z=single(bsxfun(@rdivide,Z,sqrt(sum(Z.^2))));
   end

   sigma2=sigmas(ii)*sigmas(ii);

   if (model_layer.type_kernel==0)
      model.layer{ii}.W=Z/sigma2;
      model.layer{ii}.b=-(1/sigma2)*ones(size(Z,2),1,'single');
      A=mex_exp(Z'*Z/sigma2 - 1/sigma2);
   else 
      model.layer{ii}.W=Z;
      model.layer{ii}.b=-ones(size(Z,2),1,'single');
      A=(Z'*Z);
      A=A.* A;
   end
   [U S]=mex_eig(A);
   S=max(real(S),0)+lambda2;
   model.layer{ii}.W2=bsxfun(@times,U,S'.^(-1/2))*U';
%   model.layer{ii}.W3=bsxfun(@times,U,S'.^(-1/4))*U'; 
   clear X;
   clear model_layer;
end

end

