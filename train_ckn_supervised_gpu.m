function [] = main(npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_init,lambda,lambda2,alt_optim,it_eval,init_rate,ndecrease_rate,nepochs,data_augment,device,threads,dataset)
if isdeployed
   npatches=str2num(npatches)
   subsampling=str2num(subsampling)
   nfilters=str2num(nfilters)
   sigmas=str2num(sigmas)
   type_kernel=str2num(type_kernel)
   zero_pad=str2num(zero_pad)
   centering=str2num(centering)
   whitening=str2num(whitening)
   type_init=str2num(type_init)
   lambda=str2num(lambda)
   lambda2=str2num(lambda2)
   alt_optim=str2num(alt_optim)
   it_eval=str2num(it_eval)
   init_rate=str2num(init_rate)
   ndecrease_rate=str2num(ndecrease_rate)
   nepochs=str2num(nepochs)
   data_augment=str2num(data_augment)
   threads=str2num(threads)
end
data_augment
if length(sigmas)==1
   sigmas=sigmas*ones(1,length(npatches));
end
if length(type_kernel)==1
   type_kernel=type_kernel*ones(1,length(npatches));
end

seed=0;
setenv('OMP_NUM_THREADS',num2str(threads));
setenv('MKL_NUM_THREADS',num2str(threads));
setenv('OMP_DYNAMIC','FALSE');
setenv('OMP_NESTED','FALSE');
setenv('MKL_DYNAMIC','FALSE');

%%%%%%%% set up the addpaths %%%%%%
if ~isdeployed
   addpath('data'); % contains functions to create the dataset
   addpath('mex');
end

%%%%%% Initialization %%%%%%%%%%%5
% set the seed to 0
nlayers=nnz(npatches);
rng(seed);
mkdir('logs');
format compact;
param.device=device;
param.threads=threads;
param.npatches=npatches;
param.subsampling=subsampling;
param.nfilters=nfilters;
param.sigmas=sigmas;
param.zero_pad=zero_pad;
param.centering=centering;
param.whitening=whitening;
param.lambda2=lambda2;
param.type_kernel=type_kernel;
param.lambda=lambda;
param.gradients=false;
param

if strcmp(dataset,'cifar-10')
   load('data/cifar10.mat');
elseif strcmp(dataset,'cifar-10_w')
   load('data/cifar_white.mat');
end

param.num_iter_kmeans=10;
param.num_train_images=-1;
if type_init==0
   param.ntrain=1000000
elseif type_init==1
   param.ntrain=100000
elseif type_init==2
   param.ntrain=100000
   param.num_train_images=5000;
elseif type_init==3
   param.ntrain=100000
   param.num_train_images=500;
elseif type_init==4
   param.ntrain=0;
elseif type_init==5
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
elseif type_init==6
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
   param.num_train_images=5000;
elseif type_init==7
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
   param.num_train_images=500;
elseif type_init==8
   param.ntrain=100000;
   param.num_iter_kmeans=2;
   param.num_train_images=500;
end

name=get_name(dataset,npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_init,lambda,lambda2);

name
savename=['logs/' name];
savename_supervised=sprintf('logs/supervised_%d_%s',lambda,name);
savename_svm=['logs/svm_supervised' name];
savename_svm=[];

% unsupervised training of the model
if exist(savename_supervised)
   return;
end
if exist(savename)
   load(savename);
else
   fprintf('Train the network\n');
   model=training_unsupervised(Xtr,Ytr,param,dataset);
%   save(savename,'model');
end

param.device=[]; % this is where you setup your gpu device
while isempty(param.device)
    [tmp gpu]=system('gpu_getIDs.sh');
    param.device=str2num(gpu);
end

n=size(Xtr,3);
nte=size(Xte,3);
nclasses=max(Ytr(:))+1;
fprintf('Initialize W and b\n');
psi=mex_encode_cudnn(Xtr,model,param);
scal_intercept=mean(sqrt(mean(psi.^2,2))); % TODO, move this inside
lambda=lambda*scal_intercept*scal_intercept*size(psi,1);

if true
   psib=[psi; scal_intercept*ones(1,size(psi,2))];
   scal_intercept
   %mex_centering(psi);
   param_miso.lambda=lambda/n;
   param_miso.seed=0;
   param_miso.threads=param.threads;
   param_miso.accelerated=true;
   param_miso.eps=1e-4;
   param_miso.loss=0;
   param_miso.non_uniform=true;
   param_miso.max_it=100*n;
   [Wb info]=mex_svm_miso(single(Ytr),psib,param_miso);
   W=Wb(1:end-1,:);
   b=scal_intercept*Wb(end,:)';
   scal_intercept
   clear psib;
   size(W)
   size(b)
end

% should allow changing these parameters
fprintf('Start learning supervised CKN\n');
paramsgd.momentum=0.9;
paramsgd.batch_size=min(max(nfilters),65536/max(nfilters)-32);
paramsgd.batch_size=128;
paramsgd.epochs=nepochs;
paramsgd.loss=1;
paramsgd.lambda=lambda/n;
paramsgd.lambda2=lambda2;
paramsgd.device=param.device;
paramsgd.threads=threads;
paramsgd.scal_intercept=scal_intercept^2;
paramsgd.updateWb=true;
paramsgd.data_augmentation=data_augment;
Ytr_bin=-ones(nclasses,n,'single');
Ytr_bin((1:nclasses:n*nclasses) + Ytr')=1;
Yte_bin=-ones(nclasses,nte,'single');
Yte_bin((1:nclasses:nte*nclasses) + Yte')=1;
paramsgd.learning_rate_mode=1; % 0 = nothing, 4 (once), 2 (always as before)
paramsgd.eta=init_rate;
paramsgd.it_decrease=ndecrease_rate;
paramsgd.it_eval=it_eval;
paramsgd.active_set=false;

paramsgd.preconditioning_model=false;
paramsgd.preconditioning_Wb=false;
if alt_optim
   paramsgd.update_Wb=false;
   paramsgd.update_model=true;
   paramsgd.update_miso=true;
else
   paramsgd.update_Wb=true;
   paramsgd.update_model=true;
   paramsgd.update_miso=false;
end
[model W b logs]=mex_train_ckn_cudnn(Xtr,Ytr_bin,Xte,Yte_bin,model,W,b,paramsgd);
save(savename_supervised,'logs');

end

function name=get_name(dataset,npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_init,lambda,lambda2);
nlayers=length(npatches);
name=sprintf(['model_%s_' repmat('%g_%g_%g_%g_%g_',[1,nlayers]) '%g_%g_%g_%g_%g_%g.mat'],dataset,npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_init,lambda,lambda2);
end
