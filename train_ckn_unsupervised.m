function [] = main(npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,gradients,type_learning,lambda2,device,threads,dataset)
if isdeployed
   npatches=str2num(npatches)
   subsampling=str2num(subsampling)
   nfilters=str2num(nfilters)
   sigmas=str2num(sigmas)
   zero_pad=str2num(zero_pad)
   centering=str2num(centering)
   whitening=str2num(whitening)
   gradients=str2num(gradients)
   type_learning=str2num(type_learning)
   type_kernel=str2num(type_kernel)
   lambda2=str2num(lambda2)
   threads=str2num(threads)
end
seed=0;
setenv('OMP_NUM_THREADS',num2str(threads));
setenv('MKL_NUM_THREADS',num2str(threads));
setenv('OMP_DYNAMIC','FALSE');
setenv('OMP_NESTED','FALSE');
setenv('MKL_DYNAMIC','FALSE');

%%%%%%%% set up the addpaths %%%%%%
if ~isdeployed
   addpath('data'); % contains functions to create the dataset
   addpath('mex');
end
 
%%%%%% Initialization %%%%%%%%%%%5
% set the seed to 0
nlayers=nnz(npatches);
rng(seed);
mkdir('logs');
format compact;
param.device=device;
param.threads=threads;
param.npatches=npatches;
param.subsampling=subsampling;
param.nfilters=nfilters;
param.sigmas=sigmas;
if length(param.sigmas)==1
   param.sigmas=param.sigmas*ones(1,nlayers,'single');
end
param.type_kernel=type_kernel;
if length(param.type_kernel)==1
   param.type_kernel=param.type_kernel*ones(1,nlayers,'single');
end
param.zero_pad=zero_pad;
param.centering=centering;
param.whitening=whitening;
param.gradients=gradients;
param.lambda2=lambda2;
param

if strcmp(dataset,'cifar-10')
   load('data/cifar10.mat');
elseif strcmp(dataset,'cifar-10_w')
   load('data/cifar_white.mat');
end

param.num_iter_kmeans=10;
param.num_train_images=-1;
type_init=type_learning;
if type_init==0
   param.ntrain=1000000
elseif type_init==1
   param.ntrain=100000
elseif type_init==2
   param.ntrain=100000
   param.num_train_images=5000;
elseif type_init==3
   param.ntrain=100000
   param.num_train_images=500;
elseif type_init==4
   param.ntrain=0;
elseif type_init==5
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
elseif type_init==6
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
   param.num_train_images=5000;
elseif type_init==7
   param.ntrain=1000000;
   param.num_iter_kmeans=0;
   param.num_train_images=500;
elseif type_init==8
   param.ntrain=100000;
   param.num_iter_kmeans=2;
   param.num_train_images=500;
end

name=get_name(dataset,npatches,subsampling,nfilters,param.sigmas,param.type_kernel,zero_pad,centering,whitening,gradients,type_learning,lambda2);

name
savename=['logs/' name];
savename_supervised=['logs/unsupervised' name];
savename_svm=['logs/svm' name];
savename_svm=[];

% unsupervised training of the model 
if exist(savename) 
   load(savename);
else
   fprintf('Train the network\n');
   model=training_unsupervised(Xtr,Ytr,param,dataset);
   save(savename,'model');
end

param.device=[]

fprintf('Start evaluating unsupervised CKN\n');
psiTr=mex_encode_cpu(Xtr,model,param);
mex_centering(psiTr);
mex_normalize(psiTr);
psiTe=mex_encode_cpu(Xte,model,param);
mex_centering(psiTe);
mex_normalize(psiTe);

% just compute test error (do not perform cross validation here)
% make sure you cross-validate the regularization parameter if you
% want to report the results.
[acc]=trainMisoSVM(psiTr,Ytr,psiTe,Yte,[],threads,seed);

end

function name=get_name(dataset,npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,gradients,type_learning,lambda2)
nlayers=length(npatches);
name=sprintf(['model_%s_' repmat('%g_%g_%g_%g_%g_',[1,nlayers]) '%g_%g_%g_%g_%g_%g.mat'],dataset,npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,gradients,type_learning,lambda2);
end

