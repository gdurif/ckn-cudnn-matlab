This is a re-implementation of the convolutional kernel network (CKN) methods introduced in

Julien Mairal
[End-to-End Kernel Learning with Supervised Convolutional Kernel Networks][1]. Adv. NIPS. 2016.

This is an almost pure C++ implementation using directly CUDA and CUDNN, along
with a Matlab interface.  The software package features both the unsupervised
and supervised variants of CKNs and is open-source with a GPLv3 license.


## Q: Why not using popular deep learning frameworks?

Today, there is no good reason for a developer to proceed with a pure
C++/CUDA/CUDNN implementation for such a project, unless he wants to 
learn how to use (and understand) low-level tools such as CUDA and CUDNN.
In the present case, this was my main motivation.

The resulting software package is therefore able to reproduce the results of the
original paper, which were obtained on CPUs, but is relatively painful to
install. More user-friendly (but slightly slower) versions will be made
available soon in Tensorflow and Pytorch.


## Installation

You need to edit the file `build.m`, and set up the paths to the CUDA, Matlab, and
CUDNN libraries on a Linux operating system. The file is also configured to
compile using the Intel C++ compiler, featuring the MKL library, but it also
possible to modify it to compile using gcc.

Typing `build` inside Matlab will compile the different mex files. To use them,
you may then have to exit Matlab, and use the script `run_matlab.sh` to run
Matlab again while pre-loading the required library.

We are aware that this operation is not user-friendly and you should wait for the
Tensorflow and Pytorch implementation if you do not manage to compile these files
yourself.


## Example files and data

The package contains two scripts `script_ckn_unsupervised.m`, which contains
a few examples of calls of the unsupervised variant of CKN, while `script_ckn_supervised.m` uses
the supervised variant (on GPUs).

To run these scripts, you need to download the CIFAR-10 dataset, and place it in the data folder.
You may download these files from [here](http://pascal.inrialpes.fr/data2/mairal/data/cifar_white.mat) (for the pre-whitened version)
or [here](http://pascal.inrialpes.fr/data2/mairal/data/cifar10.mat) (for the raw dataset).
See also [here](https://www.cs.toronto.edu/~kriz/cifar.html) for the original dataset.

For the supervised variant, we feature a 14-layer architecture that achieves about 90.6% accuracy. For the unsupervised one, we provide a two-layer architecture. By changing the number of filters, the accuracy ranges from 77.2% accuracy (with 64 and 256 filters on the first and second layer respectively) to 86.2% (with 2048 and 32768 filters).
All of these results are for a single model with no data augmentation. With data augmentation (flips + basic crops), the supervised model achieves about 92%.


## Q: Why do the results seem to be slightly better than in the original paper?

There are several reasons for that:
  * This is a reimplementation which features slightly different architectures than the original paper.
  * Whereas the results of the NIPS paper were obtained by optimizing hyper-parameters on a validation set, before reporting results on the test set (which results in about 0.4% loss in accuracy compared to optimizing hyper-parameters on the test set), this is not the case here. If you want to report the accuracy of the CKNs in your papers, please use the numbers provided in the NIPS paper.


[1]: https://hal.inria.fr/hal-01387399/document
