#ifndef APPROX_KERNEL_H
#define APPROX_KERNEL_H
#include "common.h"

#ifdef HAVE_MKL
#define SET_MKL_ST \
   const int num_threads= mkl_get_max_threads(); \
   mkl_set_num_threads(1); \
   mkl_set_dynamic(false);

#define SET_MKL_MT \
   mkl_set_num_threads(num_threads);
#else
#define SET_MKL_ST 
#define SET_MKL_MT 
#endif

template <typename T>
void fast_kmeans(const Matrix<T>& X, Matrix<T>& Z, const int num_iter = 10) {
   const int n=X.n();
   const int p=Z.n();
   if (num_iter >=0) {
      Vector<int> per;
      per.randperm(n);
      Vector<T> col1, col2;
      for (int ii=0; ii<p; ++ii) {
         X.refCol(per[ii],col1);
         Z.refCol(ii,col2);
         col2.copy(col1);
      };
   } else {
      Z.setAleat();
      Z.normalize();
   }

   for (int ii=0; ii<num_iter; ++ii) {
      printf("K-means epoch %d\n",ii+1);
      const int size_block=p;
      Vector<T> tmp(n);
      Vector<int> idx(n);
      //SET_MKL_ST
#pragma omp parallel for
      for (int jj =0; jj<n; jj+=size_block) {
         const int endblock=MIN(jj+size_block,n);
         const int length_block=endblock-jj;
         Matrix<T> subX, ZtX;
         X.refSubMat(jj,length_block,subX);
         Z.mult(subX,ZtX,true);
         Vector<T> col;
         for (int kk=0; kk<length_block; ++kk) {
            ZtX.refCol(kk,col);
            idx[jj+kk]=col.max();
            tmp[jj+kk]=col[idx[jj+kk]];
         }
      }
      //SET_MKL_MT
      Vector<int> numElem(p);
      numElem.setZeros();
      Z.setZeros();
      Vector<T> col1, col2;
      for (int jj =0; jj<n; ++jj) {
         const int ind=idx[jj];
         numElem[ind]++;
         X.refCol(jj,col1);
         Z.refCol(ind,col2);
         col2.add(col1);
      }
      for (int jj =0; jj<p; ++jj) {
         Z.refCol(jj,col1);
         if (numElem[jj]) {
            col1.normalize();
         } else {
            const int ind=tmp.min();
            tmp[ind]=1;
            X.refCol(ind,col2);
            col1.copy(col2);
         }
      }
   };
};

         
#endif // APPROX_KERNEL_H
