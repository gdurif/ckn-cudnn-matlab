#ifndef COMMON_CUDNN_H
#define COMMON_CUDNN_H
#include <cudnn.h>
#include "common.h"
#include "svm.h"
Timer time_preprocessing;
Timer time_upload;
static bool use_cpu_eig_solver = false;

void init_cuda(const int device, const bool cudnn = false, const bool cusolver = false) {
   cout << "Use Device " << device << endl;
   Timer time;
   time.start();
   checkCudaErrors(cudaSetDevice(device));
   checkCudaErrors(cublasCreate(&handle));
   if (cusolver)
      checkCusolver(cusolverDnCreate(&cusolver_handle));
   if (cudnn)
      checkCUDNN(cudnnCreate(&cudnn_handle));
   time.stop();
   cout << "Time for initialization of CUDA: " << endl;
   time.printElapsed();
};

void destroy_cuda(const bool cudnn = false, const bool cusolver = false) {
   checkCudaErrors(cublasDestroy(handle));
   if (cusolver)
      checkCusolver(cusolverDnDestroy(cusolver_handle));
   if (cudnn)
      checkCUDNN(cudnnDestroy(cudnn_handle));
};

void print_convolution3d(cudnnConvolutionDescriptor_t& conv) {
   cudnnConvolutionMode_t mode;
   cudnnDataType_t dataType;
   int pad[3];
   int u[3];
   int up[3];
   int arraysize;
   checkCUDNN(cudnnGetConvolutionNdDescriptor(conv,3,&arraysize,pad,u,up,&mode,&dataType));
   cout << "*** Print convolution characteristics ***" << endl;
   cout << "Num dims " << arraysize << endl;
   cout << "Pad " << pad[0] << " " << pad[1] << " "  << pad[2] << endl;
   cout << "U " << u[0] << " " << u[1] << " "  << u[2] << endl;
   cout << "Up " << up[0] << " " << up[1] << " "  << up[2] << endl;
   cout << "size conv: " << arraysize << endl;
   cout << "Format/Type: " << mode << " x " << dataType << endl;
};

void print_filter3d(cudnnFilterDescriptor_t& filter) {
   cudnnDataType_t dataType;
   cudnnTensorFormat_t format;
   int dims[5];
   int num_dims;
   checkCUDNN(cudnnGetFilterNdDescriptor(filter,5,&dataType,&format,&num_dims,dims));
   cout << "*** Print filter characteristics ***" << endl;
   cout << "Num dims " << num_dims << endl;
   cout << "Size " << dims[0] << "  " << dims[1] << "  " << dims[2]<< "  " << dims[3]<< "  " << dims[4]<< endl;
   cout << "Format/Type: " << format << " x " << dataType << endl;
};

void print_tensor3d(cudnnTensorDescriptor_t& tensor) {
   cudnnDataType_t dataType;
   int dims[5];
   int strides[5];
   int num_dims;
   checkCUDNN(cudnnGetTensorNdDescriptor(tensor,5,&dataType,&num_dims,dims,strides));
   cout << "*** Print tensor characteristics ***" << endl;
   cout << "Num dims " << num_dims << endl;
   cout << "Size " << dims[0] << "  " << dims[1] << "  " << dims[2]<< "  " << dims[3]<< "  " << dims[4]<< endl;
   cout << "Strides " << strides[0] << "  " << strides[1] << "  " << strides[2]<< "  " << strides[3]<< "  " << strides[4]<< endl;
   cout << "Format/Type: " << dataType << endl;
};

void print_convolution(cudnnConvolutionDescriptor_t& conv) {
   cudnnConvolutionMode_t mode;
   cudnnDataType_t type;
   int padx, pady, ux, uy, upx, upy;
   checkCUDNN(cudnnGetConvolution2dDescriptor(conv,&padx,&pady,&ux,&uy,&upx,&upy,&mode,&type));
   cout << "*** Print convolution characteristics ***" << endl;
   cout << "Size " << padx << " " << pady << " "  << ux << "  " << uy << " " << upx<< " " << upy << endl;
};

void print_filter(cudnnFilterDescriptor_t& filter) {
   cudnnDataType_t dataType;
   cudnnTensorFormat_t format;
   int k,c,h,w;
   checkCUDNN(cudnnGetFilter4dDescriptor(filter,&dataType,&format,&k,&c,&h,&w));
   cout << "*** Print filter characteristics ***" << endl;
   cout << "Input channels " << c << endl;
   cout << "Output channels " << k << endl;
   cout << "Size " << w << " x " << h << endl;
   cout << "Format/Type: " << format << " x " << dataType << endl;
};

void print_tensor(cudnnTensorDescriptor_t& tensor) {
   cudnnDataType_t dataType;
   int n,c,h,w;
   int ns,cs,hs,ws;
   checkCUDNN(cudnnGetTensor4dDescriptor(tensor,&dataType,&n,&c,&h,&w,&ns,&cs,&hs,&ws));
   cout << "*** Print tensor characteristics ***" << endl;
   cout << "Size " << n << " " << c << " "  << h << " x " << w << endl;
   cout << "Strides " << ns << " " << cs << " "  << hs << " " << ws << endl;
   cout << "Format/Type: " << dataType << endl;
};

template <typename T> 
inline T* get_cpu_pointer(const CudaMatrix<T>& mat) {
   if (mat.m() > 0) {
      T* pointer = new T[mat.m()*mat.n()];
      Matrix<T> cpu(pointer,mat.m(),mat.n());
      mat.getMatrix(cpu);
      return pointer;
   } else {
      return NULL;
   }
};

template <typename T> 
inline void set_cpu_pointer(T* pointer, CudaMatrix<T>& mat) {
   if (pointer) {
      Matrix<T> cpu(pointer,mat.m(),mat.n());
      mat.setMatrix(cpu);
   } else {
      mat.clear();
   }
};

template <typename T> 
inline T* get_cpu_pointer(const CudaVector<T>& mat) {
   if (mat.n() > 0) {
      T* pointer = new T[mat.n()];
      Vector<T> cpu(pointer,mat.n());
      mat.getVector(cpu);
      return pointer;
   } else {
      return NULL;
   }
};

template <typename T> 
inline void set_cpu_pointer(T* pointer, CudaVector<T>& mat) {
   if (pointer) {
      Vector<T> cpu(pointer,mat.n());
      mat.setVector(cpu);
   } else {
      mat.clear();
   }
};

/// the abstract network layer class

#define USING_NETWORK_LAYER \
   using NetworkLayer<T>::_hi; \
   using NetworkLayer<T>::_ho; \
   using NetworkLayer<T>::_wi; \
   using NetworkLayer<T>::_wo; \
   using NetworkLayer<T>::_ci; \
   using NetworkLayer<T>::_co; \
   using NetworkLayer<T>::_n; \
   using NetworkLayer<T>::_pinput; \
   using NetworkLayer<T>::_poutput; \
   using NetworkLayer<T>::_input; \
   using NetworkLayer<T>::_output; \
   using NetworkLayer<T>::_pdelta_input; \
   using NetworkLayer<T>::_pdelta_output; \
   using NetworkLayer<T>::_workspace_size; \
   using NetworkLayer<T>::_pworkspace; \

template <typename T> class NetworkLayer {
   public:
      NetworkLayer() { 
         _workspace_size=0; 
         _allocate_input=false;
         _allocate_output=false;
         _allocate_delta=false;
         checkCUDNN(cudnnCreateTensorDescriptor(&_input));
         checkCUDNN(cudnnCreateTensorDescriptor(&_output));
      };
      virtual ~NetworkLayer() {  
         checkCUDNN(cudnnDestroyTensorDescriptor(_input));
         checkCUDNN(cudnnDestroyTensorDescriptor(_output));
         if (_workspace_size > 0)
            checkCudaErrors(cudaFree(_pworkspace));    
         if (_allocate_output)
            checkCudaErrors(cudaFree(_poutput));
         if (_allocate_input)
            checkCudaErrors(cudaFree(_pinput));
         if (_allocate_delta) 
            checkCudaErrors(cudaFree(_pdelta_output));
      };

      void resize_workspace(const size_t size) {
         if (size > _workspace_size) {
            if (_workspace_size > 0) 
               checkCudaErrors(cudaFree(_pworkspace));    
            _workspace_size=size;
            checkCudaErrors(cudaMalloc(&_pworkspace,_workspace_size));    
         }
      };

      void allocate_delta() {
         _allocate_delta=true;
         checkCudaErrors(cudaMalloc(&_pdelta_output,sizeof(T)*_ho*_wo*_co*_n));
      };

      void upload_input(T* input, const int nim) {
         checkCudaErrors(cudaMemcpyAsync(_pinput,input,sizeof(T)*_wi*_hi*_ci*nim,cudaMemcpyHostToDevice));
      };

      virtual void set_input(const int h, const int w, const int c, const int n) {
         _wi=w; _hi=h; _ci=c; _n=n;
         cout << "Input dimensions: " << _ci << " x " <<
            _wi << " x " << _hi << " x " << _n << endl;
         checkCUDNN(cudnnSetTensor4dDescriptor(_input, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, _n, _ci, _hi, _wi));
         this->allocate_input();
      };

      virtual void set_input(NetworkLayer<T>& input) {
         _wi=input.w(); _hi=input.h(); _ci=input.p(); _n=input.n();
         cout << "Input dimensions: " << _ci << " x " <<
            _wi << " x " << _hi << " x " << _n << endl;
         checkCUDNN(cudnnSetTensor4dDescriptor(_input, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, _n, _ci, _hi, _wi));
         _pinput=input.output();
      };

      virtual void set_output(const int h, const int w, const int c) {
         _wo=w; _ho=h; _co=c;
         cout << "Output dimensions: " << _co << " x " <<
            _wo << " x " << _ho << " x " << _n << endl;
         checkCUDNN(cudnnSetTensor4dDescriptor(_output, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, _n, _co, _ho, _wo));
         this->allocate_output();
      };

      virtual void initialize_backward(T* delta = nullptr) = 0;
      virtual void backward(CudaMatrix<T>* grad = nullptr) = 0;
      virtual void forward() = 0;

      int h() const { return _ho; };
      int w() const { return _wo; };
      int p() const { return _co; };
      int hi() const { return _hi; };
      int wi() const { return _wi; };
      int ci() const { return _ci; };
      int n() const { return _n; };
      int size_map() const { return _ho*_wo*_co; };
      T* output() const { return _poutput; };
      T* delta() const { return _pdelta_output; };
      size_t workspace_size() const { return _workspace_size; };

   protected:
      int _hi;
      int _ho;
      int _wi;
      int _wo;
      int _ci;
      int _co;
      int _n;

      cudnnTensorDescriptor_t _input, _output;
      T* _pinput;
      T* _poutput;
      T* _pdelta_input;
      T* _pdelta_output;

      void* _pworkspace;
      size_t _workspace_size;

   private:
      void allocate_output() {
         _allocate_output=true;
         checkCudaErrors(cudaMalloc(&_poutput,sizeof(T)*_ho*_wo*_co*_n));
      };
      void allocate_input() {
         _allocate_input=true;
         checkCudaErrors(cudaMalloc(&_pinput,sizeof(T)*_hi*_wi*_ci*_n));
      };

      bool _allocate_input;
      bool _allocate_output;
      bool _allocate_delta;
};


template <typename T> class ConvLayer : public NetworkLayer<T> {
   public:
      USING_NETWORK_LAYER;

      ConvLayer() { _initialized_backward=false; _initialized_forward=false; };
      virtual ~ConvLayer() { 
         if (_initialized_forward) {
            checkCUDNN(cudnnDestroyTensorDescriptor(_norms));
            checkCUDNN(cudnnDestroyTensorDescriptor(_ones));
            checkCUDNN(cudnnDestroyFilterDescriptor(_filters));
            checkCUDNN(cudnnDestroyFilterDescriptor(_filters_ones));
            checkCUDNN(cudnnDestroyConvolutionDescriptor(_conv));
            checkCUDNN(cudnnDestroyConvolutionDescriptor(_conv_ones));
            checkCudaErrors(cudaFree(_pW));    
            checkCudaErrors(cudaFree(_ptmp));
            checkCudaErrors(cudaFree(_ptmp2));
            if (_type_kernel != 0)
               checkCudaErrors(cudaFree(_ptmp3));
            checkCudaErrors(cudaFree(_pnorms));
            checkCudaErrors(cudaFree(_pinv_norms));
            checkCudaErrors(cudaFree(_pones));
         }
         if (_initialized_backward) {
            checkCUDNN(cudnnDestroyFilterDescriptor(_filters_ones2));
            checkCUDNN(cudnnDestroyConvolutionDescriptor(_conv_ones2));
            checkCUDNN(cudnnDestroyTensorDescriptor(_scals));
         }
      };

      virtual void set_input(const size_t h, const size_t w, const size_t c, const size_t n, const T alpha, const int type_kernel) {
         _alpha=alpha;
         _type_kernel=type_kernel;
         cout << "Convolutional layer" << endl;
         NetworkLayer<T>::set_input(h,w,c,n);
      };

      virtual void set_input(NetworkLayer<T>& input, const T alpha, const int type_kernel) {
         _alpha=alpha;
         _type_kernel=type_kernel;
         cout << "Convolutional layer" << endl;
         NetworkLayer<T>::set_input(input);
      };

      void set_filter(const size_t e, const size_t p, const bool zeropad) {
         _initialized_forward=true;
         _zero_padding = zeropad;
         _e=e;
         NetworkLayer<T>::set_output(zeropad ? _hi : _hi - e +1,zeropad ? _wi : _wi - e +1,p);

         /// create descriptors 
         checkCUDNN(cudnnCreateTensorDescriptor(&_norms));
         checkCUDNN(cudnnCreateTensorDescriptor(&_ones));
         checkCUDNN(cudnnCreateFilterDescriptor(&_filters));
         checkCUDNN(cudnnCreateFilterDescriptor(&_filters_ones));
         checkCUDNN(cudnnCreateConvolutionDescriptor(&_conv));
         checkCUDNN(cudnnCreateConvolutionDescriptor(&_conv_ones));
         // Set tensor sizes
         checkCUDNN(cudnnSetTensor4dDescriptor(_norms, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, _n, 1, _ho, _wo));
         checkCUDNN(cudnnSetTensor4dDescriptor(_ones, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, 1, _ci, _e, _e));
         // set filters sizes
         checkCUDNN(cudnnSetFilter4dDescriptor(_filters, CUDNN_DATA_FLOAT,
                  CUDNN_TENSOR_NHWC, _co, _ci, _e, _e));
         checkCUDNN(cudnnSetFilter4dDescriptor(_filters_ones, CUDNN_DATA_FLOAT,
                  CUDNN_TENSOR_NHWC, 1, _ci, _e, _e));
         // set convolution parameters
         if (_zero_padding) {
            const int pad=e/2; // will only work with odd filters ?? or is it ok ?
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv,
                     pad, pad, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv_ones,
                     pad, pad, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
         } else {
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv,
                     0, 0, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv_ones,
                     0, 0, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
         }
         /// Set up convolution algorithm
         checkCUDNN(cudnnGetConvolutionForwardAlgorithm(cudnn_handle, _input,
                  _filters, _conv, _output,
                  CUDNN_CONVOLUTION_FWD_PREFER_FASTEST, 0, &_conv_algo));
         //_conv_algo=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;  /// TODO: perform some more tests here
         checkCUDNN(cudnnGetConvolutionForwardAlgorithm(cudnn_handle, _input,
                  _filters_ones, _conv_ones, _norms,
                  CUDNN_CONVOLUTION_FWD_PREFER_FASTEST, 0, &_conv_ones_algo));
         //_conv_ones_algo=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;

         /// Compute workspace size
         size_t size, size2 = 0;
         checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                  _input, _filters, _conv, _output, _conv_algo, &size));
         checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                  _input, _filters_ones, _conv_ones, _norms,
                  _conv_ones_algo, &size2));

         /// allocate workspace 
         this->resize_workspace(MAX(size,size2));
         checkCudaErrors(cudaMalloc(&_pones,sizeof(T)*_ci*_e*_e));
         float param_one = 1.0f;
         checkCUDNN(cudnnSetTensor(cudnn_handle,_ones,_pones,&param_one));
         checkCudaErrors(cudaMalloc(&_ptmp,sizeof(T)*MAX(_hi*_wi*_ci,_ho*_wo*_co)*_n));
         checkCudaErrors(cudaMalloc(&_ptmp2,sizeof(T)*_ho*_wo*_co*_n));
         if (_type_kernel==0) {
            _ptmp3=_ptmp2;
         } else {
            checkCudaErrors(cudaMalloc(&_ptmp3,sizeof(T)*_ho*_wo*_co*_n));
         }
         checkCudaErrors(cudaMalloc(&_pW,sizeof(T)*_ci*_e*_e*_co));
         checkCudaErrors(cudaMalloc(&_pnorms,sizeof(T)*_ho*_wo*_n));
         checkCudaErrors(cudaMalloc(&_pinv_norms,sizeof(T)*_ho*_wo*_n));
      };

      void set_data(T* W) {
         checkCudaErrors(cudaMemcpyAsync(_pW,W,sizeof(T)*_ci*_e*_e*_co,cudaMemcpyHostToDevice));
      }

      virtual void initialize_backward(T* delta = nullptr) {
         _initialized_backward=true;
         _pdelta_input=delta;
         this->allocate_delta();
         checkCUDNN(cudnnGetConvolutionBackwardDataAlgorithm(cudnn_handle, 
                  _filters,_output,_conv,_input,
                  CUDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST,0,&_conv_backward_data_algo));
         PRINT_I(_conv_backward_data_algo);
         checkCUDNN(cudnnGetConvolutionBackwardFilterAlgorithm(cudnn_handle, 
                  _input,_output,_conv,_filters,
                  CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST,0,&_conv_backward_filter_algo));
         PRINT_I(_conv_backward_filter_algo);

         checkCUDNN(cudnnCreateTensorDescriptor(&_scals));
         checkCUDNN(cudnnSetTensor4dDescriptor(_scals, CUDNN_TENSOR_NHWC,
                  CUDNN_DATA_FLOAT, _n, 1, _hi, _wi));
         checkCUDNN(cudnnCreateFilterDescriptor(&_filters_ones2));
         checkCUDNN(cudnnSetFilter4dDescriptor(_filters_ones2, CUDNN_DATA_FLOAT,
                  CUDNN_TENSOR_NHWC, 1, 1, _e, _e));
         checkCUDNN(cudnnCreateConvolutionDescriptor(&_conv_ones2));
         if (_zero_padding) {
            const int pad=_e/2; 
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv_ones2,
                     pad, pad, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
         } else {
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv_ones2,
                     _e-1, _e-1, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
         }
         _conv_ones2_algo=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;

         /// resize workspace if necessary
         size_t size;
         checkCUDNN(cudnnGetConvolutionBackwardFilterWorkspaceSize(cudnn_handle,
                  _input,_output,_conv,_filters,_conv_backward_filter_algo,&size));
         if (_pdelta_input != nullptr) {
            size_t size2=0;
            checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                     _norms, _filters_ones2, _conv_ones2, _scals,
                     _conv_ones2_algo, &size2));
            size=MAX(size,size2);
            checkCUDNN(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle,
                     _filters,_output,_conv,_input,_conv_backward_data_algo,&size2));
            size=MAX(size,size2);
         }
         this->resize_workspace(size);
         _vec_ones.resize(_co);
         _vec_ones.set(T(1.0));
      };

      virtual void forward() {
         float one = 1.0f, beta = 0.0f;

         /// compute the square of the elements
         cuda_sqr(_wi*_hi*_ci*_n,_pinput,_ptmp);

         /// compute the squared norms
         checkCUDNN(cudnnConvolutionForward(cudnn_handle, &one, _input,
                  _ptmp, _filters_ones, _pones, _conv_ones, _conv_ones_algo, 
                  _pworkspace, _workspace_size, &beta, _norms,_pnorms));

         /// compute the norms
         cuda_sqrt(_ho*_wo*_n,_pnorms);

         /// compute the inv norms
         cuda_inv_thrs(_ho*_wo*_n,_pnorms,_pinv_norms,T(EPS_NORM));

         /// perform the convolution
         checkCUDNN(cudnnConvolutionForward(cudnn_handle, &one, _input,
                  _pinput, _filters, _pW, _conv, _conv_algo,
                  _pworkspace, _workspace_size, &beta, _output,_ptmp));

         /// normalize
         checkCudaErrors(cublasSdgmm(handle,CUBLAS_SIDE_RIGHT,_co,_ho*_wo*_n,_ptmp,_co,_pinv_norms,1,_ptmp2,_co));

         /// take exponential - b
         if (_type_kernel==0) {
            cuda_add_exp(_co*_ho*_wo*_n,_ptmp2,_ptmp3,-_alpha);
         } else {
            cuda_sqr(_co*_ho*_wo*_n,_ptmp2,_ptmp3);
         }

         /// multiply back by the norm
         checkCudaErrors(cublasSdgmm(handle,CUBLAS_SIDE_RIGHT,_co,_ho*_wo*_n,_ptmp3,_co,_pnorms,1,_poutput,_co));
      };

      void backward(CudaMatrix<T>* grad = nullptr) {
         float beta = 1.0f;
         float two = 2.0f;
         float scal = _alpha/_n;
         /// compute g(U)
         /// note that here, poutput = ptmp2*diag(pnorms),   size output
         /// note that here, ptmp = output of raw convolution, size output
         cuda_mult(_wo*_ho*_co*_n,_ptmp2,_pdelta_output); // if type_kernel = 0, ptmp2 and ptmp3 contain B_j/alpha
         if (_type_kernel != 0) cublas_scal(_wo*_ho*_co*_n,&two,_ptmp2,1); // otherwise, ptmp2 contains B_j
         checkCUDNN(cudnnConvolutionBackwardFilter(cudnn_handle, &scal, _input,
                  _pinput, _output, _ptmp2, _conv, _conv_backward_filter_algo,
                  _pworkspace, _workspace_size, &beta, _filters, grad->rawX()));

         if (_pdelta_input != nullptr) {
            /// compute h2(U)
            cuda_custom4(_wo*_ho*_co*_n,_ptmp,_ptmp2,_poutput,_pdelta_output); // ptmp contains the right variable
            CudaMatrix<T> tmp_mat(_ptmp,_co,_ho*_wo*_n);
            tmp_mat.multTrans(_vec_ones,_vec_tmp);
            cuda_custom5(_ho*_wo*_n,_vec_tmp.rawX(),_pinv_norms);
            T beta_zero = 0.0f;

            checkCUDNN(cudnnConvolutionForward(cudnn_handle, &beta, _norms,
                     _vec_tmp.rawX(), _filters_ones2, _pones, _conv_ones2, _conv_ones2_algo,
                     _pworkspace, _workspace_size, &beta_zero, _scals,_ptmp));
            checkCudaErrors(cublasSdgmm(handle,CUBLAS_SIDE_RIGHT,_ci,_hi*_wi*_n,_pinput,_ci,_ptmp,1,_pdelta_input,_ci));
            /// compute h1(U) and add the result to h2(U),   note that pW = alpha pZ
            checkCUDNN(cudnnConvolutionBackwardData(cudnn_handle, &beta, 
                     _filters,_pW,_output,_ptmp2,_conv,_conv_backward_data_algo,
                     _pworkspace, _workspace_size,&beta,_input,_pdelta_input));
         }
      };

      int size_filter() const { return _e*_e*_ci; };
      T* filter() const { return _pW; };

   private:
      /// for forward pass only
      bool _initialized_forward;
      int _e;
      bool _zero_padding;
      cudnnTensorDescriptor_t _norms, _ones;
      cudnnFilterDescriptor_t _filters, _filters_ones;
      cudnnConvolutionDescriptor_t _conv, _conv_ones;
      cudnnConvolutionFwdAlgo_t _conv_algo, _conv_ones_algo;
      T* _pW;
      T* _ptmp;
      T* _ptmp2;
      T* _ptmp3;
      T* _pnorms;
      T* _pinv_norms;
      T* _pones;
      /// for backward pass only
      bool _initialized_backward;
      cudnnConvolutionBwdDataAlgo_t _conv_backward_data_algo;
      cudnnConvolutionBwdFilterAlgo_t _conv_backward_filter_algo;
      cudnnFilterDescriptor_t _filters_ones2;
      cudnnConvolutionDescriptor_t _conv_ones2;
      cudnnConvolutionFwdAlgo_t _conv_ones2_algo;
      cudnnTensorDescriptor_t _scals;
      T _alpha;
      CudaVector<T> _vec_ones, _vec_tmp;
      int _type_kernel; // 0 : exp(alpha(x'x -1 )),  1: poly (x'x)^2
};

template <typename T> class PoolLayer : public NetworkLayer<T> {
   public:
      PoolLayer() {  };
      virtual ~PoolLayer() { };

      virtual void set_input(NetworkLayer<T>& input, const int sub) = 0;

   protected:
      int _sub;
};

template <typename T> class GaussianPoolLayer : public PoolLayer<T> {
   public:
      USING_NETWORK_LAYER;
      using PoolLayer<T>::_sub; 

      GaussianPoolLayer() { _initialized_forward=false; };
      virtual ~GaussianPoolLayer() { 
         if (_initialized_forward) {
            checkCudaErrors(cudaFree(_pfilt));
            checkCUDNN(cudnnDestroyTensorDescriptor(_input_conv));
            checkCUDNN(cudnnDestroyTensorDescriptor(_output_conv));
            checkCUDNN(cudnnDestroyFilterDescriptor(_filters));
            checkCUDNN(cudnnDestroyConvolutionDescriptor(_conv));
            checkCUDNN(cudnnDestroyTensorDescriptor(_output_nchw));
            checkCUDNN(cudnnDestroyTensorDescriptor(_input_nchw));
            checkCudaErrors(cudaFree(_ptmp_input));
            checkCudaErrors(cudaFree(_ptmp_output));
            if (_one_dim_conv) {
               checkCUDNN(cudnnDestroyTensorDescriptor(_tmp_conv));
               checkCUDNN(cudnnDestroyFilterDescriptor(_filters2));
               checkCUDNN(cudnnDestroyConvolutionDescriptor(_conv2));
               checkCudaErrors(cudaFree(_ptmp_conv));
            } 
         }
      };

      virtual void set_input(NetworkLayer<T>& input, const int sub) { 
         cout << "Subsampling layer" << endl;
         NetworkLayer<T>::set_input(input);
         _sub=sub;
         _one_dim_conv=sub > 3;
         NetworkLayer<T>::set_output(ceil((double(_hi))/sub),ceil((double(_hi))/sub),_ci);
         _initialized_forward=true;

         if (_ho != _wo)
            cout << "Non-square images are not supported at the moment" << endl;
         const T sigma = _sub/sqr<T>(T(2.0));
         const INTM h2 = ((_ho-1)*_sub+1);
         const bool even = (_hi-h2) % 2 == 1;
         const INTM s2 = even ? 2*_sub : 2*_sub+1;
         const INTM h3 = h2 + s2 -1;
         const INTM pad = (h3-_hi)/2;
         T* filt = new T[s2];
         if (even) {
            for(int ii=-_sub; ii<_sub; ++ii){
               const T ind=ii+T(0.5);
               filt[ii+_sub] = exp(-(1.0/(2*sigma*sigma))*ind*ind);
            }
         } else {
            for(int ii=-_sub; ii<=_sub; ++ii){
               filt[ii+_sub] = exp(-(1.0/(2*sigma*sigma))*ii*ii);
            }
         }
         if (_one_dim_conv) {
            T sum=0;
            for(int ii=0; ii<s2; ++ii) 
               sum+=filt[ii];
            for(int ii=0; ii<s2; ++ii) 
               filt[ii] /= sum;
            checkCudaErrors(cudaMalloc(&_pfilt,sizeof(T)*s2));
            checkCudaErrors(cudaMemcpy(_pfilt,filt,sizeof(T)*s2,cudaMemcpyHostToDevice));
         } else {
            T* filt2 = new T[s2*s2];
            T sum=0;
            for(int ii=0; ii<s2; ++ii) {
               for(int jj=0; jj<s2; ++jj) {
                  filt2[ii*s2+jj]=filt[ii]*filt[jj];
                  sum+=filt2[ii*s2+jj];
               }
            }
            for(int ii=0; ii<s2*s2; ++ii) 
               filt2[ii] /= sum;
            checkCudaErrors(cudaMalloc(&_pfilt,sizeof(T)*s2*s2));
            checkCudaErrors(cudaMemcpy(_pfilt,filt2,sizeof(T)*s2*s2,cudaMemcpyHostToDevice));
            delete[](filt2);
         }
         delete[](filt);

         checkCUDNN(cudnnCreateTensorDescriptor(&_input_conv));
         checkCUDNN(cudnnCreateTensorDescriptor(&_output_conv));
         checkCUDNN(cudnnSetTensor4dDescriptor(_input_conv, CUDNN_TENSOR_NCHW, 
                  CUDNN_DATA_FLOAT, _n*_ci, 1, _hi, _wi));
         checkCUDNN(cudnnSetTensor4dDescriptor(_output_conv, CUDNN_TENSOR_NCHW,
                  CUDNN_DATA_FLOAT, _n*_ci, 1, _ho, _wo));
         checkCudaErrors(cudaMalloc(&_ptmp_input,sizeof(T)*_hi*_wi*_ci*_n));
         checkCudaErrors(cudaMalloc(&_ptmp_output,sizeof(T)*_ho*_wo*_ci*_n));
         checkCUDNN(cudnnCreateTensorDescriptor(&_input_nchw));
         checkCUDNN(cudnnSetTensor4dDescriptor(_input_nchw, CUDNN_TENSOR_NCHW,
                  CUDNN_DATA_FLOAT, _n, _ci, _hi, _wi));
         checkCUDNN(cudnnCreateTensorDescriptor(&_output_nchw));
         checkCUDNN(cudnnSetTensor4dDescriptor(_output_nchw, CUDNN_TENSOR_NCHW,
                  CUDNN_DATA_FLOAT, _n, _ci, _ho, _wo));
         checkCUDNN(cudnnCreateConvolutionDescriptor(&_conv));
         checkCUDNN(cudnnCreateFilterDescriptor(&_filters));

         size_t size=0;
         if (_one_dim_conv) {
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv,
                     pad, 0, _sub, 1, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
            checkCUDNN(cudnnCreateConvolutionDescriptor(&_conv2));
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv2,
                     0, pad, 1, _sub, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
            _conv_algo=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
            _conv_algo2=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
            checkCUDNN(cudnnSetFilter4dDescriptor(_filters, CUDNN_DATA_FLOAT,
                     CUDNN_TENSOR_NCHW, 1, 1, s2, 1));
            checkCUDNN(cudnnCreateFilterDescriptor(&_filters2));
            checkCUDNN(cudnnSetFilter4dDescriptor(_filters2, CUDNN_DATA_FLOAT,
                     CUDNN_TENSOR_NCHW, 1, 1, 1, s2));
            checkCUDNN(cudnnCreateTensorDescriptor(&_tmp_conv));
            checkCUDNN(cudnnSetTensor4dDescriptor(_tmp_conv, CUDNN_TENSOR_NCHW,
                     CUDNN_DATA_FLOAT, _n*_ci, 1, _ho, _wi));
            checkCudaErrors(cudaMalloc(&_ptmp_conv,sizeof(T)*_ho*_wi*_ci*_n));
            checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                     _input_conv, _filters, _conv, _tmp_conv, _conv_algo, &size));
            size_t size2=0;
            checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                     _tmp_conv, _filters2, _conv2, _output_conv, _conv_algo2, &size2));
            size=MAX(size,size2);
         } else {
            checkCUDNN(cudnnSetFilter4dDescriptor(_filters, CUDNN_DATA_FLOAT,
                     CUDNN_TENSOR_NCHW, 1, 1, s2, s2));
            checkCUDNN(cudnnSetConvolution2dDescriptor(_conv,
                     pad, pad, _sub, _sub, 1, 1, CUDNN_CROSS_CORRELATION,CUDNN_DATA_FLOAT));
            _conv_algo=CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
            PRINT_I(_conv_algo);
            checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle,
                     _input_conv, _filters, _conv, _output_conv, _conv_algo, &_workspace_size));
         }
         this->resize_workspace(size);
      };

      virtual void initialize_backward(T* delta = nullptr) {
         _pdelta_input=delta;
         this->allocate_delta();
         size_t size=0;
         if (_one_dim_conv) {
            //if (_ci*_n >= 65536) {
            //   _conv_backward_algo = CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;
            //   _conv_backward_algo2 = CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;
            //} else {
               _conv_backward_algo = CUDNN_CONVOLUTION_BWD_DATA_ALGO_0;
               _conv_backward_algo2 = CUDNN_CONVOLUTION_BWD_DATA_ALGO_0;
            //}
            checkCUDNN(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle,
                     _filters,_tmp_conv,_conv,_input_conv,_conv_backward_algo,&size));
            size_t size2;
            checkCUDNN(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle,
                     _filters2,_output_conv,_conv2,_tmp_conv,_conv_backward_algo2,&size2));
            size=MAX(size,size2);
         } else {
            checkCUDNN(cudnnGetConvolutionBackwardDataAlgorithm(cudnn_handle, 
                     _filters,_output_conv,_conv,_input_conv,
                     CUDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST,0,&_conv_backward_algo));
            if (_ci*_n >= 65536)
               _conv_backward_algo = CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;
            PRINT_I(_ci*_n);
            PRINT_I(_conv_backward_algo);
            checkCUDNN(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle,
                     _filters,_output_conv,_conv,_input_conv,_conv_backward_algo,&size));
         }
         this->resize_workspace(size);
      };

      virtual void forward() {
         float alpha = 1.0f, beta = 0.0f;
         /// change NHWC to NCHW
         checkCUDNN(cudnnTransformTensor(cudnn_handle,&alpha,_input,_pinput,&beta,_input_nchw,_ptmp_input));
         /// convolution spatial
         if (_one_dim_conv) {
            checkCUDNN(cudnnConvolutionForward(cudnn_handle, &alpha, _input_conv,
                     _ptmp_input, _filters, _pfilt, _conv, _conv_algo, 
                     _pworkspace, _workspace_size, &beta, _tmp_conv,_ptmp_conv));
            checkCUDNN(cudnnConvolutionForward(cudnn_handle, &alpha, _tmp_conv,
                     _ptmp_conv, _filters2, _pfilt, _conv2, _conv_algo2, 
                     _pworkspace, _workspace_size, &beta, _output_conv,_ptmp_output));
         } else {
            checkCUDNN(cudnnConvolutionForward(cudnn_handle, &alpha, _input_conv,
                     _ptmp_input, _filters, _pfilt, _conv, _conv_algo, 
                     _pworkspace, _workspace_size, &beta, _output_conv,_ptmp_output));
         }
         /// change back to NHWC
         checkCUDNN(cudnnTransformTensor(cudnn_handle,&alpha,_output_nchw,_ptmp_output,&beta,_output,_poutput));
      };

      virtual void backward(CudaMatrix<T>* grad = nullptr) {
         /// computes h(U)
         float alpha = 1.0f, beta = 0.0f;
         /// change NHWC to NCHW
         checkCUDNN(cudnnTransformTensor(cudnn_handle,&alpha,_output,_pdelta_output,&beta,_output_nchw,_ptmp_output));
         /// convolution spatial
         if (_one_dim_conv) {
            checkCUDNN(cudnnConvolutionBackwardData(cudnn_handle, &alpha,
                     _filters2, _pfilt, _output_conv,_ptmp_output, _conv2,
                     _conv_backward_algo2, _pworkspace,_workspace_size,
                     &beta,_tmp_conv,_ptmp_conv));
            checkCUDNN(cudnnConvolutionBackwardData(cudnn_handle, &alpha,
                     _filters, _pfilt, _tmp_conv,_ptmp_conv, _conv,
                     _conv_backward_algo, _pworkspace,_workspace_size,
                     &beta,_input_conv,_ptmp_input));
         } else {
            checkCUDNN(cudnnConvolutionBackwardData(cudnn_handle, &alpha,
                     _filters, _pfilt, _output_conv,_ptmp_output, _conv,
                     _conv_backward_algo, _pworkspace,_workspace_size,
                     &beta,_input_conv,_ptmp_input));
         }
         /// change back to NHWC
         checkCUDNN(cudnnTransformTensor(cudnn_handle,&alpha,_input_nchw,_ptmp_input,&beta,_input,_pdelta_input));
      };

   private:
      bool _initialized_forward;
      T* _ptmp_input;
      T* _ptmp_output;
      T* _pfilt;
      cudnnTensorDescriptor_t _input_nchw, _input_conv, _output_nchw, _output_conv;
      cudnnFilterDescriptor_t _filters;
      cudnnConvolutionDescriptor_t _conv;
      cudnnConvolutionFwdAlgo_t _conv_algo;

      /// variables for the for backward pass only
      cudnnConvolutionBwdDataAlgo_t _conv_backward_algo;

      /// variables for one_dimensional_filtering
      bool _one_dim_conv;
      cudnnFilterDescriptor_t _filters2;
      cudnnConvolutionDescriptor_t _conv2;
      cudnnConvolutionFwdAlgo_t _conv_algo2;
      cudnnTensorDescriptor_t _tmp_conv;
      cudnnConvolutionBwdDataAlgo_t _conv_backward_algo2;
      T* _ptmp_conv;
};


template <typename T> class SimplePoolLayer : public PoolLayer<T> {
   public:
      USING_NETWORK_LAYER;
      using PoolLayer<T>::_sub; 

      SimplePoolLayer() { _initialized_forward=false; };
      virtual ~SimplePoolLayer() { 
         if (_initialized_forward) {
            checkCUDNN(cudnnDestroyPoolingDescriptor(_pooler));
         }
      };

      virtual void set_input(NetworkLayer<T>& input, const int sub) { 
         cout << "Simple Subsampling layer" << endl;
         NetworkLayer<T>::set_input(input);
         _sub=sub;
         NetworkLayer<T>::set_output(ceil((double(_hi))/sub),ceil((double(_hi))/sub),_ci);
         _initialized_forward=true;
         const INTM pady = ceil((_wo*_sub-_wi)/T(2.0));
         const INTM padx = ceil((_ho*_sub-_hi)/T(2.0));
         checkCUDNN(cudnnCreatePoolingDescriptor(&_pooler));
         checkCUDNN(cudnnSetPooling2dDescriptor(_pooler,static_cast<cudnnPoolingMode_t>(POOL_AVERAGE),CUDNN_PROPAGATE_NAN,_sub,_sub,padx,pady,_sub,_sub));
      };

      virtual void initialize_backward(T* delta = nullptr) {
         _pdelta_input=delta;
         this->allocate_delta();
      };

      virtual void forward() {
         float alpha = 1.0f, beta = 0.0f;
         checkCUDNN(cudnnPoolingForward(cudnn_handle,_pooler,&alpha,_input,_pinput,&beta,_output,_poutput));
      };

      virtual void backward(CudaMatrix<T>* grad = nullptr) {
         /// computes h(U)
         float alpha = 1.0f, beta = 0.0f;
         checkCUDNN(cudnnPoolingBackward(cudnn_handle,_pooler,&alpha,_output,_poutput,_output,_pdelta_output,_input,_pinput,&beta,_input,_pdelta_input));
      };

   private:
      bool _initialized_forward;
      cudnnPoolingDescriptor_t _pooler;
};

template <typename T> class DummyPoolLayer : public PoolLayer<T> {
   public:
      USING_NETWORK_LAYER;
      using PoolLayer<T>::_sub; 

      DummyPoolLayer() {  };
      virtual ~DummyPoolLayer() { };

      virtual void set_input(NetworkLayer<T>& input, const int sub) { 
         cout << "No Subsampling layer" << endl;
         NetworkLayer<T>::set_input(input);
         _wo=_wi; _ho=_hi; _co=_ci; _sub=sub;
         _poutput=_pinput;
      };

      virtual void initialize_backward(T* delta) {
         _pdelta_input=delta;
         _pdelta_output=delta;
      };

      virtual void forward() { };

      virtual void backward(CudaMatrix<T>* grad = nullptr) { };
};

// k(Z'Z)^{-1/2} 

template <typename T> class MultLayer : public NetworkLayer<T> {
   public:
      USING_NETWORK_LAYER;

      MultLayer() {  };
      virtual ~MultLayer() { };

      virtual void set_input(NetworkLayer<T>& input, const T alpha, const int type_kernel) { 
         cout << "Mult layer" << endl;
         _alpha=alpha;
         _type_kernel=type_kernel;
         NetworkLayer<T>::set_input(input);
         NetworkLayer<T>::set_output(_hi,_wi,_ci);
      };

      virtual void forward() {
         CudaMatrix<T> input(_pinput,_ci,_hi*_wi*_n);
         CudaMatrix<T> output(_poutput,_ci,_hi*_wi*_n);
         _W2.mult(input,output);
      };

      virtual void backward(CudaMatrix<T>* grad = nullptr) {
         /// computes h(U)
         CudaMatrix<T> delta_input(_pdelta_input,_ci,_hi*_wi*_n);
         CudaMatrix<T> delta_output(_pdelta_output,_ci,_hi*_wi*_n);
         _W2.mult(delta_output,delta_input);

         /// computes g(U) w.r.t. Z
         CudaMatrix<T> tmp, tmp2;
         CudaMatrix<T> input(_pinput,_ci,_hi*_wi*_n);
         input.mult(delta_output,tmp,false,true);
         tmp.addTranspose(tmp2);
         _W4.mult(tmp2,tmp,true);
         tmp.mult(_W4,tmp2);
         tmp2.mult_elementWise(_W5);
         _W4.mult(tmp2,tmp);
         tmp.mult(_W4,tmp2,false,true);
         tmp2.mult_elementWise(_W3);
         _W1.mult(tmp2,*grad,false,false,-T(1.0)/(_alpha*_n));  // this is grad w.r.t Z
      };

      virtual void initialize_backward(T* delta) {
         _pdelta_input=delta;
         this->allocate_delta();
      };

      virtual void set_data(T* pW, const int m) {
         _W1.setData(pW,m,_ci);
      };

      void recompute_W234(const T lambda2) {
         // W1 = alpha Z
         _W1.mult(_W1,_W5,true,false,T(1.0)/_alpha); // W1 = alpha Z
         if (_type_kernel==0) {
            _W5.add_exp(-_alpha); 
            _W3.copy(_W5);
            _W3.scal(_alpha);
         } else {
            _W3.copy(_W5);
            _W3.scal(2);
            _W5.sqr();
         }
         // W3 = k'(Z'Z)
         CudaVector<T> S;
         _W5.eigSym(_W4,S,use_cpu_eig_solver);  // W4 = U
         Vector<T> Svec;
         S.getVector(Svec);
         const int n=Svec.n();
         Matrix<T> C(n,n);
         Svec.add(lambda2);
         Svec.Sqrt();
         for (int ii=0; ii<n; ++ii)
            for (int jj=0; jj<n; ++jj)
               C(ii,jj)=1./((Svec[ii]*Svec[jj])* (Svec[ii]+Svec[jj]));
         Svec.Sqrt();
         Svec.inv();
         S.setVector(Svec);
         _W5.copy(_W4); 
         _W5.multDiagRight(S);
         _W5.mult(_W5,_W2,false,true); // W2 = k(Z'Z)^{-1/2}
         _W5.setMatrix(C);  // W5 = C
      };

      virtual void do_gradient_step(CudaMatrix<T>& grad, const T eta, const bool precond = false) {
         CudaVector<T> tmp, tmp2;
         if (_ones.n()==0) {
            _ones.resize(_W1.m());
            _ones.set(T(1.0));
         }
         if (precond) {
            CudaMatrix<T> AZ, tmp_mat;  // W = alpha Z
            _precond.mult(_W1,AZ,false,false,T(1.0)/_alpha);
            AZ.mult_elementWise(grad,tmp_mat);
            tmp_mat.multTrans(_ones,tmp,_alpha);
            AZ.mult_elementWise(_W1,tmp_mat);
            tmp_mat.multTrans(_ones,tmp2);
            tmp.div(tmp2);
            AZ.multDiagRight(tmp);
            _precond.mult(grad,AZ,false,false,T(1.0),-T(1.0));
            _W1.add(AZ,-eta*_alpha);
         } else {
            _W1.add(grad,-eta*_alpha);
         }
         _W1.sqr(grad);
         grad.multTrans(_ones,tmp,T(1.0)/(_alpha*_alpha));
         tmp.inv_sqrt();
         _W1.multDiagRight(tmp);
      };

      void initialize_preconditioning() {
         this->initialize_preconditioning(_W1);
      };

      void initialize_preconditioning(const CudaMatrix<T>& W) {
         CudaMatrix<T> C, U;
         CudaVector<T> S;
         W.mult(W,C,false,true,T(1.0)/W.n());
         C.eigSym(U,S,use_cpu_eig_solver);
         const int n = S.n();
         const T sum = cublas_asum(n,S.rawX(),1);
         Vector<T> copyS;
         S.getVector(copyS);
         cuda_inv_sqrt_thrs(n,S.rawX(),S.rawX(),S.rawX(),T(0.1)*static_cast<T>(sqrt(copyS.maxval())));
         const T mean = cublas_asum(n,S.rawX(),1)/n;
         C.copy(U);
         //U.multDiagRight(S);
         U.mult(C,_precond,false,true,T(1.0)/mean);
      };

      void get_parameters(Layer<T>& layer) {
         _W1.getMatrix(layer.W);
         _W2.getMatrix(layer.W2);
      };

      virtual void saveState(List<T*>& list)  { 
         list.push_back(get_cpu_pointer(_W1));
      };

      virtual void uploadState(List<T*>& list)  {  // remove ?
         set_cpu_pointer(list.front(),_W1);
         list.pop_front();
      };

      virtual void uploadState(ListIterator<T*>& it)  { 
         set_cpu_pointer(*it,_W1);
         ++it;
      };


   private:
      CudaMatrix<T> _W1, _W2, _W3, _W4, _W5;
      CudaVector<T> _ones;
      T _alpha;
      int _type_kernel;
      CudaMatrix<T> _precond;
};

#define USING_PREDICTION_LAYER \
   using PredictionLayer<T>::_psi; \
using PredictionLayer<T>::_gamma; \
using PredictionLayer<T>::_gamma2; \
using PredictionLayer<T>::_delta; \
using PredictionLayer<T>::_ones; \
using PredictionLayer<T>::_ones2; \
using PredictionLayer<T>::_W; \
using PredictionLayer<T>::_Y; \
using PredictionLayer<T>::_b; \
using PredictionLayer<T>::_is_bias; 

template <typename T> class PredictionLayer {
   public:
      PredictionLayer() : _is_bias(true) {  };
      virtual ~PredictionLayer() { };

      virtual void set_input(NetworkLayer<T>& input) {
         const int m = input.size_map();
         const int n = input.n();
         _ones.resize(n);
         _ones.set(T(1.0));
         _ones2.resize(m);
         _ones2.set(T(1.0));
         _psi.setData(input.output(),m,n);
      };

      virtual void initialize_backward(T* delta) {
         _delta.setData(delta,_psi.m(),_psi.n());
      };

      virtual void initialize_forward(const Matrix<T>& W, const Vector<T>& b, const T scal_intercept) { 
         _W.setMatrix(W);
         if (_is_bias) {
            _scal_intercept=scal_intercept;
            _b.setVector(b);
         }
      };

      virtual void forward() {
         _W.mult(_psi,_gamma,true,false);
         if (_is_bias)
            _gamma.rank1Update(_b,_ones);
      };

      virtual void upload_labels(T* Y, const int nim) {
         _Y.resize(_W.n(),_psi.n());
         checkCudaErrors(cudaMemcpyAsync(_Y.rawX(),Y,sizeof(T)*_W.n()*nim,cudaMemcpyHostToDevice));
      };

      void do_gradient_step(CudaMatrix<T>& gradW, CudaVector<T>& gradb, const T eta, const T lambda) {
         const int K = _W.n();
         gradW.add(_W,lambda/K);
         if (_is_bias) 
            gradb.add(_b,lambda/(_scal_intercept*K));
         _W.add(gradW,-eta);
         if (_is_bias) 
            _b.add(gradb,-eta*_scal_intercept*_scal_intercept);
      };

      void get_parameters(Matrix<T>& W, Vector<T>& b) {
         _W.getMatrix(W);
         if (_is_bias) _b.getVector(b);
      };

      void get_predictions(Matrix<T>& gamma) {
         _gamma.getMatrix(gamma);
      }

      T val_regularization() {
         return _is_bias ? T(0.5)*(_W.normFsq() + _b.nrm2sq()/_scal_intercept)/_W.n() : T(0.5)*_W.normFsq()/_W.n();
      };

      bool is_bias() const { return _is_bias; };

      virtual void backward(CudaMatrix<T>& gradW, CudaVector<T>& gradb) = 0;  
      virtual T loss(const int nim) = 0; 

      virtual void saveState(List<T*>& list)  { 
         list.push_back(get_cpu_pointer(_W));
         list.push_back(get_cpu_pointer(_b));
      };

      virtual void uploadState(List<T*>& list)  {  // remove ?
         set_cpu_pointer(list.front(),_W);
         list.pop_front();
         set_cpu_pointer(list.front(),_b);
         list.pop_front();
      };

      virtual void uploadState(ListIterator<T*>& it)  { 
         set_cpu_pointer(*it,_W);
         ++it;
         set_cpu_pointer(*it,_b);
         ++it;
      };

   protected:
      CudaMatrix<T> _psi, _gamma, _delta, _W, _Y, _gamma2;
      CudaVector<T> _ones, _ones2, _b;
      bool _is_bias;
      T _scal_intercept;
};


template <typename T> class SqHingeLossLayer : public PredictionLayer<T> {
   public:
      USING_PREDICTION_LAYER; 

      SqHingeLossLayer(const bool is_bias) { _is_bias=is_bias; };
      virtual ~SqHingeLossLayer() { };

      virtual void backward(CudaMatrix<T>& gradW, CudaVector<T>& gradb) {
         cuda_custom3(_gamma.m()*_gamma.n(),_gamma.rawX(),_Y.rawX(),T(-1.0/_Y.m()));
         _psi.mult(_gamma,gradW,false,true,T(1.0)/_psi.n());
         if (_is_bias)
            _gamma.mult(_ones,gradb,T(1.0)/_psi.n());
         _W.mult(_gamma,_delta); /// we assume W is centered  
      };

      virtual T loss(const int nim) {  /// should be called after forward, not after backward
         _gamma2.resize(_gamma.m(),_gamma.n());
         cuda_custom6(_gamma.m()*nim,_gamma.rawX(),_Y.rawX(),_gamma2.rawX());  
         return T(0.5)*cublas_asum<T>(_gamma.m()*nim,_gamma2.rawX(),1)/(_Y.m()*nim);
      };

   private:
      CudaVector<T> _tmp;
};

template <typename T> class SquareLossLayer : public PredictionLayer<T> {
   public:
      USING_PREDICTION_LAYER; 

      SquareLossLayer() {  };
      SquareLossLayer(const bool is_bias) { _is_bias=is_bias; };
      virtual ~SquareLossLayer() { };

      virtual void backward(CudaMatrix<T>& gradW, CudaVector<T>& gradb) {
         _gamma.add(_Y,-T(1.0));
         _psi.mult(_gamma,gradW,false,true,T(1.0)/(_Y.m()*_psi.n()));
         if (_is_bias)
            _gamma.mult(_ones,gradb,T(1.0)/(_Y.m()*_psi.n()));
         _W.mult(_gamma,_delta);
      };

      virtual T loss(const int nim) {
         cuda_custom7(_gamma.m()*nim,_gamma.rawX(),_Y.rawX(),_gamma2.rawX());
         return cublas_asum<T>(_gamma.m()*nim,_gamma2.rawX(),1)/(_Y.m()*nim);
      };
};

template <typename T> class SquareLossConv1x1Layer : public SquareLossLayer<T> {
   public:
      USING_PREDICTION_LAYER; 

      SquareLossConv1x1Layer(const int nchannels) { 
         _is_bias=false; 
         _nchannels=nchannels;
      };
      virtual ~SquareLossConv1x1Layer() { };

      virtual void forward() {
         const int m = _psi.m();
         const int n = _psi.n();
         _sizemap = m/_nchannels;
         _gamma.resize(_sizemap,n);
         CudaMatrix<T> psi(_psi.rawX(),_nchannels,n*_sizemap);
         CudaVector<T> gamma_vec(_gamma.rawX(),_sizemap*n);
         CudaVector<T> W_vec(_W.rawX(),_sizemap*n);
         psi.multTrans(W_vec,gamma_vec);
      };

      virtual void backward(CudaMatrix<T>& gradW, CudaVector<T>& gradb) {
         _gamma.add(_Y,-T(1.0));
         const int m = _psi.m();
         const int n = _psi.n();
         gradW.resize(_nchannels,1);
         CudaMatrix<T> psi(_psi.rawX(),_nchannels,n*_sizemap);
         CudaVector<T> gamma_vec(_gamma.rawX(),_sizemap*n);
         CudaVector<T> gradW_vec(gradW.rawX(),_nchannels);
         CudaVector<T> W_vec(_W.rawX(),_nchannels);
         psi.mult(gamma_vec,gradW_vec,T(1.0)/(_sizemap*n));
         CudaMatrix<T> delta(_delta.rawX(),_nchannels,n*_sizemap);
         delta.rank1Update(W_vec,gamma_vec);
      };

      virtual void upload_labels(T* Y, const int nim) {
         _Y.resize(_sizemap,_psi.n());
         checkCudaErrors(cudaMemcpyAsync(_Y.rawX(),Y,sizeof(T)*_sizemap*nim,cudaMemcpyHostToDevice));
      };

   private:
      int _nchannels;
      int _sizemap;
};

template <typename T> class Network {
   public:
      Network(Layer<T> layers[],const int nlayers, const int hi, const int wi,
            const int ci, const int n, const T lambda2 = 0) {
         _nlayers=nlayers;
         _conv_layers = new ConvLayer<T>[nlayers];
         _pool_layers = new PoolLayer<T>*[nlayers];
         _mult_layers = new MultLayer<T>[nlayers];
         _gradients = new CudaMatrix<T>[nlayers];
         _gradients_acc = new CudaMatrix<T>[nlayers];
         _lambda2=lambda2;
         _n=n;
         _nim=n;
         _centering = layers[0].type_layer==1 || layers[0].type_layer==5;
         _whitening = layers[0].type_layer==5;
         for (int ii=0; ii<nlayers; ++ii) {
            Layer<T>& layer = layers[ii];
            const int type_kernel = layers[ii].type_kernel;
            const T alpha = type_kernel == 0 ? T(1.0)/(layers[ii].sigma*layers[ii].sigma) : T(1.0);
            const int p = layer.W.n();
            const int sub = layer.subsampling;

            /// set up the convolutional layer
            if (ii==0 && !_centering && !_whitening) {
               _conv_layers[ii].set_input(hi,wi,ci,n,alpha,type_kernel);
               _conv_layers[ii].set_filter(layer.npatch,p,layer.zero_padding);
            } else if (ii == 0 && (_centering || _whitening)) {
               _npatch=layer.npatch;
               _zero_padding=layer.zero_padding;
               _c=ci;
               _h=hi;
               _w=wi;
               if (_zero_padding) {
                  _conv_layers[ii].set_input(hi,wi,ci*_npatch*_npatch,n,alpha,type_kernel);
               } else {
                  _conv_layers[ii].set_input(hi-_npatch+1,wi-_npatch+1,ci*_npatch*_npatch,n,alpha,type_kernel);
               }
               _conv_layers[ii].set_filter(1,p,layer.zero_padding);
            } else {
               _conv_layers[ii].set_input(_mult_layers[ii-1],alpha,type_kernel);
               _conv_layers[ii].set_filter(layer.npatch,p,layer.zero_padding);
            } 
            _conv_layers[ii].set_data(layer.W.rawX());

            /// set up the pooling layer
            if (sub==1) {
               _pool_layers[ii]=new DummyPoolLayer<T>();
            } else if (layers[ii].pooling_mode==POOL_GAUSSIAN_FILTER) {
               _pool_layers[ii]=new GaussianPoolLayer<T>();
            } else {
               _pool_layers[ii]=new SimplePoolLayer<T>();
            }
            _pool_layers[ii]->set_input(_conv_layers[ii],sub);

            /// set up the projection layer
            _mult_layers[ii].set_input(*_pool_layers[ii],alpha,type_kernel);
            _mult_layers[ii].set_data(_conv_layers[ii].filter(),_conv_layers[ii].size_filter());
            _mult_layers[ii].recompute_W234(_lambda2);
         }
      };

      virtual ~Network() {
         delete[](_conv_layers);
         for (int ii=0; ii<_nlayers; ++ii) {
            delete(_pool_layers[ii]);
         }
         delete[](_pool_layers);
         delete[](_mult_layers);
         delete[](_gradients);
         delete[](_gradients_acc);
      };

      virtual void forward(T* input_data, const int nim) {
         _nim=nim;
         if (_centering || _whitening) {
            time_preprocessing.start();
            const int m = _c*_npatch*_npatch;
            const int size = _conv_layers[0].wi()*_conv_layers[0].hi();
            Matrix<T> XX(m,size*nim);
#pragma omp parallel for
            for (int ii=0; ii<nim; ++ii) {
               Matrix<T> X;
               XX.refSubMat(size*ii,size,X);
               Map<T> map(input_data+_c*_h*_w*ii,_c,_h,_w);
               map.im2col(X,_npatch,_zero_padding);
               centering(X,_c);
               if (_whitening)
                  whitening(X);
            }
            time_preprocessing.stop();
            time_upload.start();
            _conv_layers[0].upload_input(XX.rawX(),nim);
            time_upload.stop();
         } else {
            time_upload.start();
            _conv_layers[0].upload_input(input_data,nim);
            time_upload.stop();
         }
         for (int jj=0; jj<_nlayers; ++jj) {
            _conv_layers[jj].forward();
            _pool_layers[jj]->forward();
            _mult_layers[jj].forward();
         }
      };

      virtual void get_output(T* output, const int nim) {
         checkCudaErrors(cudaMemcpy(output,_mult_layers[_nlayers-1].output(),sizeof(T)*_mult_layers[_nlayers-1].size_map()*nim,cudaMemcpyDeviceToHost));
      };

      virtual void backward() {  /// note, this function does not work if nim != batch size
         // TODO: stop backward if gradient is zero?
         for (int jj=_nlayers-1; jj>= 0; --jj) {
            _mult_layers[jj].backward(&_gradients[jj]);
            _pool_layers[jj]->backward();
            _conv_layers[jj].backward(&_gradients[jj]);
         }
      };

      virtual void initialize_backward(Layer<T> layers[]) { 
         for (int jj=0; jj<_nlayers; ++jj)  {
            if (jj==0) {
               _conv_layers[0].initialize_backward();
            } else {
               _conv_layers[jj].initialize_backward(_mult_layers[jj-1].delta());
            }
            _pool_layers[jj]->initialize_backward(_conv_layers[jj].delta());
            _mult_layers[jj].initialize_backward(_pool_layers[jj]->delta());
         };
      };

      virtual void initialize_preconditioning_model() {
         for (int jj=0; jj<_nlayers; ++jj) {
            _mult_layers[jj].initialize_preconditioning();
         }
      };


//      virtual void get_gradients(Layer<T> layers[]) {
//         for (int jj=0; jj<_nlayers; ++jj) _gradients[jj].getMatrix(layers[jj].gradW);
//      };

      virtual T loss() = 0; 

      virtual void do_gradient_step(const T eta, const T momentum,const bool precond = false) {
         if (_gradients_acc[0].m()==0) {
            for (int ii=0; ii<_nlayers; ++ii) 
               _gradients_acc[ii].copy(_gradients[ii]);
         } else {
            for (int ii=0; ii<_nlayers; ++ii) {
               _gradients_acc[ii].scal(momentum);
               _gradients_acc[ii].add(_gradients[ii],T(1.0)-momentum);
               _gradients[ii].copy(_gradients_acc[ii]);
            }
         }
         for (int ii = 0; ii<_nlayers; ++ii) 
            _mult_layers[ii].do_gradient_step(_gradients[ii],eta,precond); 
      };

      virtual void recompute_W234() {
         for (int ii = 0; ii<_nlayers; ++ii)
            _mult_layers[ii].recompute_W234(_lambda2); 
      }

      virtual void get_parameters(Layer<T> layers[]) {
         for (int ii = 0; ii<_nlayers; ++ii)
            _mult_layers[ii].get_parameters(layers[ii]);
      };

      virtual void saveState(List<T*>& list)  { 
         for (ListIterator<T*> it = list.begin(); it != list.end(); ++it)
            delete[](*it);
         list.clear();
         for (int ii=0; ii<_nlayers; ++ii) 
            _mult_layers[ii].saveState(list);
         for (int ii=0; ii<_nlayers; ++ii) 
            list.push_back(get_cpu_pointer(_gradients_acc[ii]));
      };

      virtual void uploadState(ListIterator<T*>& it)  { 
         for (int ii=0; ii<_nlayers; ++ii) 
            _mult_layers[ii].uploadState(it);
         for (int ii=0; ii<_nlayers; ++ii) {
            set_cpu_pointer(*it,_gradients_acc[ii]);
            ++it;
         }
         this->recompute_W234();
      };

      int output_size() const { return _mult_layers[_nlayers-1].size_map(); };
      T* output() { return _mult_layers[_nlayers-1].output(); };
      int n() const { return _n; };

   protected:
      int _nlayers;
      int _n;
      int _nim;
      ConvLayer<T>* _conv_layers;
      PoolLayer<T>** _pool_layers;
      MultLayer<T>* _mult_layers;
      CudaMatrix<T>* _gradients;
      CudaMatrix<T>* _gradients_acc;
      T _lambda2;
      bool _centering;
      bool _whitening;
      int _h;
      int _w;
      int _c;
      int _npatch;
      bool _zero_padding;
};

template <typename T> class SupervisedNetwork : public Network<T> {
   public:
      using Network<T>::_mult_layers; 
      using Network<T>::_nlayers; 

      SupervisedNetwork(Layer<T> layers[],const int nlayers, const int hi, const int wi,
            const int ci, const int n, const T lambda2 = 0, const loss_t loss = SQLOSS, const bool is_bias = true) : Network<T>(layers,nlayers,hi,wi,ci,n,lambda2) {
         _loss=loss;
         /// set up the prediction layer
         if (loss == SQLOSS) {
            _prediction_layer = new SquareLossLayer<T>(is_bias);
         } else if (loss == SQLOSS_CONV) {
            _prediction_layer = new SquareLossConv1x1Layer<T>(_mult_layers[_nlayers-1].p());
         } else if (loss == ONE_VS_ALL_SQHINGE_LOSS) {
            _prediction_layer = new SqHingeLossLayer<T>(is_bias);
         }
         _prediction_layer->set_input(_mult_layers[_nlayers-1]);
      };

      virtual ~SupervisedNetwork() {
         delete(_prediction_layer);
      };

      virtual void forward_predictions() {
         _prediction_layer->forward();
      };

      virtual void backward(T* Y, const int nim) {  /// note, this function does not work if nim != batch size
         _prediction_layer->upload_labels(Y,nim);
         _prediction_layer->backward(_gradW,_gradb);
         Network<T>::backward();
      };

      virtual void initialize_forward(const Matrix<T>& W, const Vector<T>& b, const T scal_intercept) { 
         _prediction_layer->initialize_forward(W,b,scal_intercept);
      };

      virtual void initialize_backward(Layer<T> layers[]) { 
         Network<T>::initialize_backward(layers);
         _prediction_layer->initialize_backward(_mult_layers[_nlayers-1].delta());
      };

/*      virtual void get_gradients(Matrix<T>& gradW, Vector<T>& gradb, Layer<T> layers[]) {
         Network<T>::get_gradients(layers);
         _gradW.getMatrix(gradW);
         if (_prediction_layer->is_bias())
            _gradb.getVector(gradb);
      };*/

      virtual void upload_labels(T* Y) {
         _prediction_layer->upload_labels(Y,this->_nim);
      };

      virtual void get_predictions(Matrix<T>& pred) {
         _prediction_layer->get_predictions(pred);
      }

      virtual T loss() {
         return _prediction_layer->loss(this->_nim);
      };

      virtual void do_gradient_step(const T eta, const T momentum, const T lambda, const bool update_model = true, const bool update_Wb = true, const bool precond_model = false) {
         if (update_Wb) {
            if (_gradW_acc.m()==0) {
               _gradW_acc.copy(_gradW);
               if (_prediction_layer->is_bias())
                  _gradb_acc.copy(_gradb);
            } else {
               _gradW_acc.scal(momentum);
               _gradW_acc.add(_gradW,T(1.0)-momentum);
               _gradW.copy(_gradW_acc);
               if (_prediction_layer->is_bias()) {
                  _gradb_acc.scal(momentum);
                  _gradb_acc.add(_gradb,T(1.0)-momentum);
                  _gradb.copy(_gradb_acc);
               }
            }
            _prediction_layer->do_gradient_step(_gradW,_gradb,eta,lambda);
         }
         if (update_model)
            Network<T>::do_gradient_step(eta,momentum,precond_model);
      };

      virtual void get_parameters(Layer<T> layers[], Matrix<T>& W, Vector<T>& b) {
         Network<T>::get_parameters(layers);
         _prediction_layer->get_parameters(W,b);
      };

      virtual T val_regularization() {
         return _prediction_layer->val_regularization();
      };

      virtual void saveState(List<T*>& list)  { 
         Network<T>::saveState(list);
         _prediction_layer->saveState(list);
         list.push_back(get_cpu_pointer(_gradW_acc));
         list.push_back(get_cpu_pointer(_gradb_acc));
      };

      virtual void uploadState(ListIterator<T*>& it)  { 
         Network<T>::uploadState(it);
         _prediction_layer->uploadState(it);
         set_cpu_pointer(*it,_gradW_acc);
         ++it;
         set_cpu_pointer(*it,_gradb_acc);
         ++it;
      };

   private:
      CudaMatrix<T> _gradW;
      CudaVector<T> _gradb; 
      CudaMatrix<T> _gradW_acc;
      CudaVector<T> _gradb_acc; 
      loss_t _loss;
      PredictionLayer<T>* _prediction_layer;
};

template <typename Tin, typename T>
inline void encode_ckn_cudnn(const Map<Tin>& maps, Layer<T> layers[], const int nlayers, Matrix<T>& psi, const int batch_size) {
   Timer time, time2, time3;
   time_preprocessing.reset();
   time_upload.reset();
   time.start();
   const int n = maps.z();
   const int nchannels=maps.y()/maps.x();
   int batchsize=MIN(batch_size,n);
   SupervisedNetwork<T> network(layers,nlayers,maps.x(),maps.x(),nchannels,batchsize);
   psi.resize(network.output_size(),n);
   T* input_data = new T[maps.x()*maps.y()*batchsize];

   for (int ii=0; ii<n; ii+=batchsize) {
      /// get input data
      const int nim = MIN(n,ii+batchsize) - ii;
      Tin* input_in = maps.rawX()+maps.x()*maps.y()*ii;
      convert_image_data_map_switch<Tin,T>(input_in,input_data,maps.x()*maps.y()/nchannels,nchannels,nim); 

      /// forward pass
      time2.start();
      network.forward(input_data,nim);
      time2.stop();

      /// get back the data on cpu 
      time3.start();
      network.get_output(psi.rawX()+ii*psi.m(),nim);
      time3.stop();
   }

   delete[](input_data);
   cout << "Time doing computations, including data upload" << endl;
   time2.printElapsed();
   cout << "Time downloading output" << endl;
   time3.printElapsed();
   cout << "Time pre-processing" << endl;
   time_preprocessing.printElapsed();
   cout << "Time uploading" << endl;
   time_upload.printElapsed();
   cout << "Total time" << endl;
   time.printElapsed();
};

template <typename T>
struct ParamSGD {
   T lambda;
   T lambda2;
   loss_t loss;
   int epochs;
   int batch_size;
   T momentum;
   T eta;
   T scal_intercept;
   bool preconditioning_model;
   bool update_Wb;
   bool update_model;
   bool update_miso;
   int learning_rate_mode;
   bool active_set;
   int it_eval;
   int it_decrease;
   bool data_augmentation;
};

template <typename Tin, typename T>
inline void eval_loss_classif(const Map<Tin>& maps, const Matrix<T>& Y,
      SupervisedNetwork<T>& network, T* input, const T lambda, T& loss, T& classif) {
   const int n = Y.n();
   const int batch_size=network.n();
   const int size_map=maps.x()*maps.y();
   const int nchannels=maps.y()/maps.x();
   const int nclasses=Y.m();
   loss=0;
   classif=0;
   for (int ii=0; ii<n; ii+=batch_size) {
      const int nim = MIN(n,ii+batch_size)-ii;
      convert_image_data_map_switch<Tin,T>(maps.rawX()+size_map*ii,
            input,size_map/nchannels,nchannels,nim);
      network.forward(input,nim);
      network.forward_predictions();
      Matrix<T> pred;
      network.get_predictions(pred);
      Vector<T> losses(nim);
      Vector<T> classifs(nim);
#pragma omp parallel for
      for (int jj=0; jj<nim; ++jj) {
         losses[jj]=0;
         Vector<T> colY, colpred;
         pred.refCol(jj,colpred);
         Y.refCol(ii+jj,colY);
         for (int kk=0; kk<nclasses; ++kk) {
            const T s = MAX(1-colY[kk]*colpred[kk],0);
            losses[jj]+= T(0.5)*s*s;
         }
         losses[jj] /= nclasses;
         classifs[jj] = colY.max() == colpred.max();
      }
      loss += losses.asum();
      classif += classifs.asum();
   };
   classif /= n;
   loss /= n;
   loss += lambda*network.val_regularization();
};


template <typename Tin, typename T>
inline void eval_loss_classif_psi(const Map<Tin>& maps, const Matrix<T>& Y,
      SupervisedNetwork<T>& network, T* input, const T lambda, T& loss, T& classif,
      Matrix<T>& psi, Vector<bool>& active_set, const Vector<int>& ind_active_set, const bool psi_with_intercept = false, const bool get_one_gradient = false) {
   const int n = Y.n();
   const int nactive = ind_active_set.n();
   const int batch_size=network.n();
   const int size_map=maps.x()*maps.y();
   const int nchannels=maps.y()/maps.x();
   const int nclasses=Y.m();
   loss=0;
   classif=0;
   for (int ii=0; ii<nactive; ii+=batch_size) {
      const int nim = MIN(nactive,ii+batch_size)-ii;
      for (int kk=0; kk<nim; ++kk) {
         convert_image_data_map_switch<Tin,T>(maps.rawX()+size_map*ind_active_set[ii+kk],
               input+kk*size_map,size_map/nchannels,nchannels,1);
      }
      network.forward(input,nim);
      if (psi_with_intercept) {
         Matrix<T> psiI(psi.m()-1,nim);
         network.get_output(psiI.rawX(),nim);
         for (int jj=0; jj<nim; ++jj)
            memcpy(psi.rawX()+(ii+jj)*psi.m(),psiI.rawX()+jj*psiI.m(),sizeof(T)*psiI.m());
      } else {
         network.get_output(psi.rawX()+ii*psi.m(),nim);
      }
      network.forward_predictions();
      if (ii==0 && get_one_gradient) {
         int nlabels=Y.m();
         T* labels=new T[nlabels*nim];
         for (int kk=0; kk<nim; ++kk) {
            memcpy(labels+kk*nlabels,Y.rawX()+ind_active_set[ii+kk]*nlabels,sizeof(T)*nlabels);
         }
         network.backward(labels,nim);
         delete[](labels);
      }
      Matrix<T> pred;
      network.get_predictions(pred);
      Vector<T> losses(nim);
      Vector<T> classifs(nim);
#pragma omp parallel for
      for (int jj=0; jj<nim; ++jj) {
         losses[jj]=0;
         Vector<T> colY, colpred;
         pred.refCol(jj,colpred);
         Y.refCol(ind_active_set[ii+jj],colY);
         for (int kk=0; kk<nclasses; ++kk) {
            const T s = MAX(1-colY[kk]*colpred[kk],0);
            losses[jj]+= T(0.5)*s*s;
         }
         active_set[ind_active_set[ii+jj]]= losses[jj] != 0;
         losses[jj] /= nclasses;
         classifs[jj] = colY.max() == colpred.max();
      }
      loss += losses.asum();
      classif += classifs.asum();
   };
   classif /= n;
   loss /= n;
   loss += lambda*network.val_regularization();
};

template <typename Tin, typename T>
inline T eval_loss(const Map<Tin>& maps, const Matrix<T>& Y, SupervisedNetwork<T>& network, T* input, const T lambda) {
   const int n = Y.n();
   const int batch_size=network.n();
   const int size_map=maps.x()*maps.y();
   const int nchannels=maps.y()/maps.x();
   T loss=0;
   for (int ii=0; ii<n; ii+=batch_size) {
      const int nim = MIN(n,ii+batch_size)-ii;
      convert_image_data_map_switch<Tin,T>(maps.rawX()+size_map*ii,
            input,size_map/nchannels,nchannels,nim);
      network.forward(input,nim);
      network.forward_predictions();
      network.upload_labels(Y.rawX()+Y.m()*ii);
      loss += nim*network.loss();
   };
   loss /= n;
   loss += lambda*network.val_regularization();
   return loss;
};

/// TODO: store log files, code restart
/// TODO: normalize pre-conditioning of gradients? 
/// TODO: add data augmentation

template <typename T>
void select_solver_eigenvalue(Network<T>* network) {
   cerr << "Benchmarking eigenvalue solver on cpu" << endl;
   Timer time_bench;
   time_bench.start();
   use_cpu_eig_solver=true;
   for (int ii=0; ii<5; ++ii)
      network->recompute_W234();
   time_bench.printElapsed();
   T cpu_score = time_bench.getElapsed();
   time_bench.stop();
   time_bench.reset();
   time_bench.start();
   use_cpu_eig_solver=false;
   for (int ii=0; ii<5; ++ii)
      network->recompute_W234();
   time_bench.stop();
   time_bench.printElapsed();
   T gpu_score = time_bench.getElapsed();
   use_cpu_eig_solver = gpu_score > cpu_score;
};

void update_active_set(Vector<bool>& active_set, Vector<int>& ind_active_set, const bool reactivate = false) {
   const int n = active_set.n();
   int nactive=0;
   for (int ii=0; ii<n; ++ii)
      if (active_set[ii]) ++nactive;
   if (reactivate) {
      for (int ii=0; ii<n; ++ii) {
         if (!active_set[ii] && static_cast<double>(random())/RAND_MAX > 0.8) {
            active_set[ii]=true;
            ++nactive;
         }
      }
   }
   ind_active_set.resize(nactive);
   nactive=0;
   for (int ii=0; ii<n; ++ii)
      if (active_set[ii]) ind_active_set[nactive++]=ii;
   cerr << "Size of active set: " << nactive << endl; 
};

template <typename Tin, typename T>
inline void sgd_solver_supervised(const Map<Tin>& maps, const Matrix<T>& Y, const Map<Tin>& maps_val, const Matrix<T>& Yval, Layer<T> layers[], const int nlayers, Matrix<T>& W, Vector<T>& b, const ParamSGD<T>& param, Matrix<T>& logs) {
   const int n = maps.z();
   const int nchannels=maps.y()/maps.x();
   const int batch_size=MIN(param.batch_size,n);
   const int size_map=maps.x()*maps.y();
   Vector<int> per;
   const bool is_bias = b.n() > 0;
   const int nlabels = Y.m();
   Timer time, time2, time3, time4, time5, time6, time7;
   use_cpu_eig_solver=true;
   logs.resize(3,param.epochs);
   logs.setZeros();

   /// initialization
   cerr << "**********************************" << endl;
   cerr << "SGD solver for SCKN" << endl;
   cerr << "batch size: " << batch_size << endl;
   cerr << "momentum: " << param.momentum << endl;
   cerr << "eta: " << param.eta << endl;
   cerr << "loss: " << param.loss << endl;
   cerr << "lr mode: " << param.learning_rate_mode << endl;
   cerr << "lambda: " << param.lambda << ", lambda2: " << param.lambda2 << endl;
   if (param.update_miso) 
      cerr << "Use miso algorithm on cpu" << endl;
   if (param.update_model) {
      cerr << "updating the model"  << endl;
      if (param.preconditioning_model) 
         cerr << "Model Preconditioning activated "  << endl;
   }
   if (param.active_set)
      cerr << "Active set heuristic is on" << endl;
   if (param.update_Wb) {
      cerr << "updating the prediction layer"  << endl;
   }
   cerr << "**********************************" << endl;
   time5.start();
   T* input_data = new T[maps.x()*maps.y()*batch_size];
   T* input_labels = new T[nlabels*batch_size];
   SupervisedNetwork<T> network(layers,nlayers,maps.x(),maps.x(),nchannels,batch_size,param.lambda2,param.loss,is_bias);
   network.initialize_backward(layers);
   network.initialize_forward(W,b,param.scal_intercept);
   select_solver_eigenvalue(&network);
   List<T*> state; 
   network.saveState(state);
   T best_loss=0;
   T scal_eta=T(1.0);
   int counter=0;
   Vector<bool> active_set(n);
   active_set.set(true);
   Vector<int> ind_active_set;
   update_active_set(active_set,ind_active_set);
   Vector<bool> best_active_set(n);
   best_active_set.copy(active_set);
   bool init_ok=false;
   time5.stop();

   // check restart
   bool restart=false;
   int first_epoch=0;
   /// todo: upload state and so on if needed
   /// copy also the "best" variables

   if (param.preconditioning_model) 
      network.initialize_preconditioning_model();
   /// solver starts here
   for (int ii=first_epoch; ii<param.epochs; ++ii) {
      //save_on_disk(state,ii,counter,scal_eta,logs,active_set);
      cerr << "Epoch " << ii << endl;

      T old_loss, loss, loss_val, classif, classif_val; //eval_loss_classif
      if (!restart && (ii % param.it_eval == 0)) {
         if (param.update_miso) {
            int nactive=ind_active_set.n();
            const int nclasses=W.n();
            const int m = W.m();
            Matrix<T> psi(m+1,nactive);
            for (int jj=0; jj<nactive; ++jj)
               psi(m,jj)=sqrt(param.scal_intercept);
            Vector<T> ynum(nactive);
            Vector<T> col;
            for (int jj=0; jj<nactive; ++jj) {
               Y.refCol(ind_active_set[jj],col);
               ynum[jj] = col.max();
            }
            time.start();
            eval_loss_classif_psi(maps,Y,network,input_data,param.lambda,loss,classif,psi,active_set,ind_active_set,true,ii==0); 
            if (param.active_set) update_active_set(active_set,ind_active_set,true);
            time.stop();
            Vector<int> info;
            Vector<T> objs;
            Matrix<T> Wb(m+1,Y.n());
            time7.start();
            miso_svm_onevsrest(ynum,psi,Wb,param.lambda*n/T(nactive),T(1e-4),500*n,info,objs,true,false,true,false);  
            for (int jj=0; jj<nclasses; ++jj) {
               for (int kk=0; kk<m; ++kk)
                  W(kk,jj)=Wb(kk,jj);
               b[jj]=sqrt(param.scal_intercept)*Wb(m,jj);
            }
            network.initialize_forward(W,b,param.scal_intercept);
            time7.stop();
            old_loss=loss;
            loss=objs.mean()*nactive/n;
            cerr << "  Number of iterations for MISO: " << static_cast<T>(info.sum())/info.n() << endl;
            cerr << "  Intermediate loss on training set: " << old_loss << endl;
            cerr << "  Intermediate classification on training set: " << classif << endl;
         } else {
            /// useless for optimization, just for monitoring loss... not compatible with active set
            eval_loss_classif(maps,Y,network,input_data,param.lambda,loss,classif);
         }
         eval_loss_classif(maps_val,Yval,network,input_data,param.lambda,loss_val,classif_val);
         cerr << "  Loss on training set: " << loss << endl;
         cerr << "  Loss on validation set: " << loss_val << endl;
         cerr << "  Classification on validation set: " << classif_val << endl;
         logs(0,ii)=loss;
         logs(1,ii)=loss_val;
         logs(2,ii)=classif_val;
         cerr << "********************************" << endl;
         if (false && ii > 0 && param.learning_rate_mode > 0 && param.learning_rate_mode != 10 &&
               (param.learning_rate_mode <= 2 || !init_ok) && 
               (loss > best_loss 
                || (param.learning_rate_mode % 2 ==1 && old_loss > best_loss))) {
            cerr << "Increase of loss, perform back-tracking" << endl;
            network.uploadState(state.begin());
            scal_eta /= 2;
            cerr << "New step size: " << param.eta*scal_eta << endl;
            counter=0;
            if (param.active_set) {
               active_set.copy(best_active_set);
               update_active_set(active_set,ind_active_set);
            }
            cerr << "********************************" << endl;
         } else {
            if (ii > 0) init_ok=true;
            network.saveState(state);
            best_loss = loss;
            best_active_set.copy(active_set);
         }
      }
      if (ii > 0 && counter >= param.it_decrease)  {
         if (param.learning_rate_mode == 0 || param.learning_rate_mode ==2) {
            scal_eta /= 2;
         } else {
            scal_eta /= 10;
         }
         cerr << "New step size: " << param.eta*scal_eta << endl;
         if (param.learning_rate_mode <= 1)
            counter=0;
         cerr << "********************************" << endl;
      }
      ++counter;

      int nactive=ind_active_set.n();
      per.randperm(nactive);
      const int iter_per_epochs = ceil(nactive/batch_size);
      for (int jj=0; jj<iter_per_epochs; ++jj) {
         time2.start();
         /// get the data and perform the forward pass
         for (int kk=0; kk<batch_size; ++kk) {
            convert_image_data_map_switch<Tin,T>(maps.rawX()+size_map*ind_active_set[per[jj*batch_size+kk % nactive]],
                  input_data+kk*size_map,size_map/nchannels,nchannels,1,param.data_augmentation);
         }

         network.forward(input_data,batch_size);
         network.forward_predictions();
         time2.stop();

         time3.start();
         /// perform the backward pass
         for (int kk=0; kk<batch_size; ++kk)
            memcpy(input_labels+kk*nlabels,Y.rawX()+ind_active_set[per[jj*batch_size+kk % nactive]]*nlabels,sizeof(T)*nlabels);
         network.backward(input_labels,batch_size);
         time3.stop();

         time4.start();
         /// do gradient step
         network.do_gradient_step((param.eta*scal_eta*nactive)/n,param.momentum,param.lambda,param.update_model,
               param.update_Wb,param.preconditioning_model);
         time4.stop();
         time6.start();
         network.recompute_W234();
         time6.stop();
      }
   }

   // get the network parameters
   network.get_parameters(layers,W,b);

   cout << "Time for initialization" << endl;
   time5.printElapsed();
   cout << "Time for forward pass, including data upload" << endl;
   time2.printElapsed();
   cout << "Time for backward pass" << endl;
   time3.printElapsed();
   cout << "Time for gradient step" << endl;
   time4.printElapsed();
   cout << "Time for W234" << endl;
   time6.printElapsed();
   if (param.update_miso) {
      cout << "Time for MISO update" << endl;
      time7.printElapsed();
   }
   cout << "Total for loss evaluation" << endl;
   time.printElapsed();

   delete[](input_data);
   delete[](input_labels);
   for (ListIterator<T*> it = state.begin(); it != state.end(); ++it)
      delete[](*it);
};

#endif
// vim: set softtabstop=3 shiftwidth=3 expandtab :
